package nobworks.acode.client;

import nobworks.acode.client.proto.AcodeTabSet;
import nobworks.acode.shared.Release;
import nobworks.zcore.zclass.ZBrowserUtil;
import nobworks.zsgwt.zclass.ZEntryPoint;
import nobworks.zsgwt.zclass.ZVersion;

public class Acode extends ZEntryPoint {
	public void onModuleLoad() {
		ZBrowserUtil.consoleLog( Release.build );
		ZVersion.show();
		zEntryInit();
		baseLayout.addMember( new AcodeTabSet() );
	}
}
