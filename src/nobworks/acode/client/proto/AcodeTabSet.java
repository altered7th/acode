package nobworks.acode.client.proto;

import com.smartgwt.client.widgets.tab.Tab;

import nobworks.acode.client.common.AppTabSet;
import nobworks.acode.client.testtab.TestMain;
import nobworks.zcore.zclass.icons.IconUtil;

public class AcodeTabSet extends AppTabSet {

	public AcodeTabSet() {
		createUI();
	}
	private void createUI() {
		Tab testTab = addTab( "テスト", IconUtil.icons.bug(), new TestMain() );
	}
}
