package nobworks.acode.client.facade;

import com.google.gwt.core.client.GWT;
import com.smartgwt.client.util.SC;

import nobworks.acode.client.common.AppUtil;
import nobworks.acode.client.common.FacadeBusy;
import nobworks.zcore.zclass.ZBrowserUtil;
import nobworks.zcore.zclass.ZListener;
import nobworks.zcore.zclass.ZObject;
import nobworks.zcore.zclass.ZService;
import nobworks.zcore.zclass.ZServiceAsync;
import nobworks.zcore.zclass.ZServiceCallback;
import nobworks.zcore.zclass.ZUtil;
import nobworks.zsgwt.zclass.ZBusyLight;

/*
 * ZEntryPoint.zservice は ZEntryPoint で宣言されており、SmartGWT パッケージをインポートしている。
 * SmartGWTmobile でアクセスすると動作が不安定なため、FacadeBase に独立させ
 * SmartGWT と SmartGWTmobile の両方からアクセスできるようにする。
 */
public class FacadeBase {
	public final static ZServiceAsync zservice = GWT.create( ZService.class );
	private String serviceClass;
	
	public FacadeBase( String serviceClass ) {
		this.serviceClass = serviceClass;
	}
	protected String getServiceClass() {
		return serviceClass;
	}
	protected ZObject createZObject( String methodName, Class<?> clazz, String message ) {
		return AppUtil.createZObject( serviceClass, methodName, clazz, message ); 
	}
	
	@SuppressWarnings("unchecked")
	protected void fire( ZObject para, ZListener callback ) {
		FacadeBusy.on();
		zservice.fire( para, new ZServiceCallback().onResult( result-> {
			dumpError( result );
			FacadeBusy.off();
			ZObject zobj = (ZObject)result;
			callback.onSuccess( zobj );
		} ) );
	}

	protected static void dumpError( ZObject para ) {
		String errorMessage = para.getErrorMessage();
		if ( ZUtil.isEmpty( errorMessage ) ) {
			return;
		}
		ZBrowserUtil.consoleLog( errorMessage );
	}
}
