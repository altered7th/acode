package nobworks.acode.client.facade;

import nobworks.zcore.zclass.ZListener;
import nobworks.zcore.zclass.ZObject;

public class TestDaoFacade extends FacadeBase {

	public TestDaoFacade() {
		super( "nobworks.acode.server.zql.DaoService" );
	}

	public void getRow( ZListener callback ) {
		ZObject para = createZObject( "testGetRow", getClass(), "testGetRow" );	
		fire( para, result-> {
			ZObject zobj = (ZObject)result;
			callback.onSuccess( zobj );
		} );
	}
	public void getRows( ZListener callback ) {
		ZObject para = createZObject( "testGetRows", getClass(), "testGetRows" );	
		fire( para, result-> {
			ZObject zobj = (ZObject)result;
			callback.onSuccess( zobj );
		} );
	}
	public void fileSql( ZListener callback ) {
		ZObject para = createZObject( "testFileSql", getClass(), "testFileSql" );	
		fire( para, result-> {
			ZObject zobj = (ZObject)result;
			callback.onSuccess( zobj );
		} );
	}
	public void createMockData( ZListener callback ) {
		ZObject para = createZObject( "testWrite", getClass(), "testWrite" );	
		fire( para, result-> {
			ZObject zobj = (ZObject)result;
			callback.onSuccess( zobj );
		} );
	}
}
