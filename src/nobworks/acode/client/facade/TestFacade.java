package nobworks.acode.client.facade;

import com.google.gwt.core.client.GWT;

import nobworks.zcore.zclass.ZListener;
import nobworks.zcore.zclass.ZObject;

public class TestFacade extends FacadeBase {

	public TestFacade() {
		super( "nobworks.acode.server.graphql.ShopMasterService" );
	}

	/*
	public void testH2Connection( ZListener callback ) {
		ZObject para = createZObject( "testH2Connection", getClass(), "testH2Connection" );	
		fire( para, result-> {
			ZObject zobj = (ZObject)result;
			callback.onSuccess( zobj );
		} );
	}
	*/
	
	public void getAllShops( ZListener callback ) {
		ZObject para = createZObject( "getAllShops", getClass(), "getAllShops" );	
		fire( para, result-> {
			ZObject zobj = (ZObject)result;
			callback.onSuccess( zobj );
		} );
	}
	public void getShop( String shopNo, ZListener callback ) {
		ZObject para = createZObject( "getShop", getClass(), "getShop" );	
		para.set( "shopNo", shopNo );
		fire( para, result-> {
			ZObject zobj = (ZObject)result;
			callback.onSuccess( zobj );
		} );
	}
	public void writeShop( ZListener callback ) {
		ZObject para = createZObject( "writeShop", getClass(), "writeShop" );	
		fire( para, result-> {
			ZObject zobj = (ZObject)result;
			callback.onSuccess( zobj );
		} );
	}
	public void writeShops( ZListener callback ) {
		ZObject para = createZObject( "writeShops", getClass(), "writeShops" );	
		fire( para, result-> {
			ZObject zobj = (ZObject)result;
			callback.onSuccess( zobj );
		} );
	}
	//---------------------------------------------------------------------
	// URLConnection
	public void urlWriteShop( String scriptFile, ZListener callback ) {
		ZObject para = createZObject( "writeShopServlet", getClass(), "writeShopServlet" );	
		para.set( "scriptFile", scriptFile );
		fire( para, result-> {
			ZObject zobj = (ZObject)result;
			callback.onSuccess( zobj );
		} );
	}
	public void urlGetShop( String shopNo, ZListener callback ) {
		ZObject para = createZObject( "urlGetShop", getClass(), "urlGetShop" );	
		para.set( "shopNo", shopNo );
		fire( para, result-> {
			ZObject zobj = (ZObject)result;
			callback.onSuccess( zobj );
		} );
	}
	public void urlGetAllShops( ZListener callback ) {
		ZObject para = createZObject( "urlGetAllShops", getClass(), "urlGetAllShops" );
		fire( para, result-> {
			ZObject zobj = (ZObject)result;
			callback.onSuccess( zobj );
		} );
	}
	public void urlWriteShops( ZListener callback ) {
		ZObject para = createZObject( "urlWriteShops", getClass(), "urlWriteShops" );
		fire( para, result-> {
			ZObject zobj = (ZObject)result;
			callback.onSuccess( zobj );
		} );
	}
	//---------------------------------------------------------------------
	// zql
}
