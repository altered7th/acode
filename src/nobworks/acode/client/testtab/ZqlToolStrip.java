package nobworks.acode.client.testtab;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

import nobworks.acode.client.common.AppUtil;
import nobworks.acode.client.facade.TestDaoFacade;
import nobworks.zcore.zclass.ZObject;
import nobworks.zcore.zclass.ZUtil;
import nobworks.zcore.zclass.icons.IconUtil;
import nobworks.zql.client.ZqlDto;

public class ZqlToolStrip extends ToolStrip {

	private TextAreaItem textArea;

	public ZqlToolStrip() {
		setWidth100();
		createUI();
	}
	public void setTextArea( TextAreaItem textArea ) {
		this.textArea = textArea;
	}
	private void createUI() {
		addSpacer( 3 );
		title();
		addSeparator();
		mockData();
		getRow();
		getRows();
		fileSql();
	}
	private void mockData() {
		ToolStripButton button = AppUtil.createToolStripButton( "mockData", IconUtil.icons.database_lightning() );
		addMember( button );	
		
		button.addClickHandler( event-> {
			new TestDaoFacade().createMockData( callback-> {
				ZObject para = (ZObject)callback;
				addTextValue( "Mock data created" );
			});
		});	
	}
	private void getRow() {
		ToolStripButton button = AppUtil.createToolStripButton( "getRow", IconUtil.icons.database_lightning() );
		addMember( button );	
		
		button.addClickHandler( event-> {
			new TestDaoFacade().getRow( callback-> {
				ZObject para = (ZObject)callback;
				ZqlDto dto = (ZqlDto)para.get( "dto" );
//				dto.printValuesClient();
				String s = dumpString( dto );
				addTextValue( s );
			});
		});	
	}
	private void getRows() {
		ToolStripButton button = AppUtil.createToolStripButton( "getRows", IconUtil.icons.database_lightning() );
		addMember( button );	
		
		button.addClickHandler( event-> {
			new TestDaoFacade().getRows( callback-> {
				ZObject para = (ZObject)callback;
				List<ZqlDto> dtoList = (List<ZqlDto>)para.get( "dtoList" );
				String s = dumpString( dtoList );
				addTextValue( s );
			});
		});	
	}
	private void fileSql() {
		ToolStripButton button = AppUtil.createToolStripButton( "fileSql", IconUtil.icons.database_lightning() );
		addMember( button );	
		
		button.addClickHandler( event-> {
			new TestDaoFacade().fileSql( callback-> {
				ZObject para = (ZObject)callback;
				List<ZqlDto> dtoList = (List<ZqlDto>)para.get( "dtoList" );
				String s = dumpString( dtoList );
				addTextValue( s );
			});
		});	
	}
	private String dumpString( List<ZqlDto> dtoList ) {
		StringBuffer sb = new StringBuffer();
		int row = 1;
		for ( ZqlDto dto : dtoList ) {
			if ( row > 1 ) {
				sb.append( "\n" );
			}
			sb.append( "row=" + row + "\n" );
			sb.append( dumpString( dto ) );
			row++;
		}
		return sb.toString();
	}
	private String dumpString( ZqlDto dto ) {
		StringBuffer sb = new StringBuffer();
		Map<String,Object> dtoMap = dto.getMap();
		Set<String> names = dtoMap.keySet();
		for ( String name : names ) {
			Object value = dtoMap.get( name );
			String dataType = value.getClass().getSimpleName();
			if ( sb.length() > 0 ) {
				sb.append( "\n" );
			}
			sb.append( "[ZqlDto] " + name + "<" + dataType + ">" + ":" + dtoMap.get( name ) );
		}
		return sb.toString();
	}
	private void addTextValue( String value ) {
		String x = textArea.getValueAsString();
		x = value + "\n" + ZUtil.convNull( x );
		textArea.setValue( x );
	}
	private void title() {
		HLayout hLayout = new HLayout();
		hLayout.setWidth( 10 );
		hLayout.setHeight( 25 );
		hLayout.setAlign( Alignment.CENTER );
//		hLayout.setBorder( "1px solid gray" );

		Label titleLabel = new Label( "zql" );
		titleLabel.setAutoWidth();
		titleLabel.setHeight( 25 );
		titleLabel.setWrap( false );
		hLayout.addMember( titleLabel );
		addMember( hLayout );
	}
}
