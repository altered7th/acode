package nobworks.acode.client.testtab;

import com.google.gwt.core.client.GWT;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.HTMLFlow;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

import nobworks.acode.client.common.AppUtil;
import nobworks.acode.client.facade.TestFacade;
import nobworks.zcore.zclass.ZObject;
import nobworks.zcore.zclass.ZUtil;
import nobworks.zcore.zclass.icons.IconUtil;

public class UrlConnectionToolStrip extends ToolStrip {

	private TextAreaItem textArea;
	private TextItem shopNoTextItem;
	
	public UrlConnectionToolStrip() {
		setWidth100();
		createUI();
	}
	public void setTextArea( TextAreaItem textArea ) {
		this.textArea = textArea;
	}
	private void createUI() {
		addSpacer( 3 );
		title();
		addSeparator();
		writeShop1();
		writeShop2();
		getShop();
		getAllShops();
		writeShops();
	}
	private void writeShops() {
		ToolStripButton button = AppUtil.createToolStripButton( "writeShops", IconUtil.icons.world() );
		addMember( button );
		
		button.addClickHandler( event-> {
			new TestFacade().urlWriteShops( callback-> {
				ZObject para = (ZObject)callback;
				String json = para.getString( "json" );
				addTextValue( json );
			});
		});	
	}
	private void getAllShops() {
		ToolStripButton button = AppUtil.createToolStripButton( "getAllShops", IconUtil.icons.world() );
		addMember( button );
		
		button.addClickHandler( event-> {
			new TestFacade().urlGetAllShops( callback-> {
				ZObject para = (ZObject)callback;
				String json = para.getString( "json" );
				addTextValue( json );
			});
		});	
	}
	private void getShop() {
		shopNoTextItem = new TextItem();
		shopNoTextItem.setTitle( "ShopNo" );
		addFormItem( shopNoTextItem );

		ToolStripButton button = AppUtil.createToolStripButton( "Search", IconUtil.icons.zoom() );
		addMember( button );
		
		button.addClickHandler( event-> {
			String shopNo = shopNoTextItem.getValueAsString();
			new TestFacade().urlGetShop( shopNo, callback-> {
				ZObject para = (ZObject)callback;
				String json = para.getString( "json" );
				addTextValue( json );
			});
		});
	}
	private void writeShop1() {
		ToolStripButton button = AppUtil.createToolStripButton( "writeShop1", IconUtil.icons.world() );
		addMember( button );
		
		button.addClickHandler( event-> {
			new TestFacade().urlWriteShop( "writeShop1.gql", callback-> {
				ZObject para = (ZObject)callback;
				String json = para.getString( "json" );
				addTextValue( json );
			});
		});	
	}
	private void writeShop2() {
		ToolStripButton button = AppUtil.createToolStripButton( "writeShop2", IconUtil.icons.world() );
		addMember( button );
		
		button.addClickHandler( event-> {
			new TestFacade().urlWriteShop( "writeShop2.gql", callback-> {
				ZObject para = (ZObject)callback;
				String json = para.getString( "json" );
				addTextValue( json );
			});
		});	
	}
	
	
	private void addTextValue( String value ) {
		String x = textArea.getValueAsString();
		x = value + "\n" + ZUtil.convNull( x );
		textArea.setValue( x );
	}
	private void title() {
		HLayout hLayout = new HLayout();
		hLayout.setWidth( 10 );
		hLayout.setHeight( 25 );
		hLayout.setAlign( Alignment.CENTER );
//		hLayout.setBorder( "1px solid gray" );

		Label titleLabel = new Label( "URLConnection" );
		titleLabel.setAutoWidth();
		titleLabel.setHeight( 25 );
		titleLabel.setWrap( false );
		hLayout.addMember( titleLabel );
		addMember( hLayout );
	}
}
