package nobworks.acode.client.testtab;

import com.google.gwt.core.client.GWT;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.HTMLFlow;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

import nobworks.acode.client.common.AppUtil;
import nobworks.acode.client.facade.TestFacade;
import nobworks.zcore.zclass.ZObject;
import nobworks.zcore.zclass.ZUtil;
import nobworks.zcore.zclass.icons.IconUtil;

public class TestToolStrip extends ToolStrip {

	private TextAreaItem textArea;
	private TextItem shopNoTextItem;
	
	public TestToolStrip() {
		setWidth100();
		createUI();
	}
	public void setTextArea( TextAreaItem textArea ) {
		this.textArea = textArea;
	}
	private void createUI() {
		clearButton();
		allShops();
		writeShopButton();
		getShop();
		writeShopsButton();
		h2ConnectionButton();
	}
	private void allShops() {
		ToolStripButton button = AppUtil.createToolStripButton( "allShops", IconUtil.icons.database_go() );
		addMember( button );
		
		button.addClickHandler( event-> {
			new TestFacade().getAllShops( callback-> {
				ZObject para = (ZObject)callback;
				String json = para.getString( "json" );
				addTextValue( json );
			});
		});	
	}
	private void getShop() {
		shopNoTextItem = new TextItem();
		shopNoTextItem.setTitle( "ShopNo" );
		addFormItem( shopNoTextItem );

		ToolStripButton button = AppUtil.createToolStripButton( "Search", IconUtil.icons.zoom() );
		addMember( button );
		
		button.addClickHandler( event-> {
			String shopNo = shopNoTextItem.getValueAsString();
			new TestFacade().getShop( shopNo, callback-> {
				ZObject para = (ZObject)callback;
				String json = para.getString( "json" );
				addTextValue( json );
			});
		});
	}
	private void writeShopsButton() {
		ToolStripButton button = AppUtil.createToolStripButton( "writeShops", IconUtil.icons.database_go() );
		addMember( button );
		
		button.addClickHandler( event-> {
			new TestFacade().writeShops( callback-> {
				ZObject para = (ZObject)callback;
				String json = para.getString( "json" );
				addTextValue( json );
			});
		});
	}
	private void writeShopButton() {
		ToolStripButton button = AppUtil.createToolStripButton( "writeShop", IconUtil.icons.database_go() );
		addMember( button );
		
		button.addClickHandler( event-> {
			new TestFacade().writeShop( callback-> {
				ZObject para = (ZObject)callback;
				String json = para.getString( "json" );
				addTextValue( json );
			});
		});
	}
	private void h2ConnectionButton() {
		ToolStripButton button = AppUtil.createToolStripButton( "H2Connection", IconUtil.icons.database_go() );
		addMember( button );
		
		button.addClickHandler( event-> {
		/*	new TestFacade().testH2Connection( callback-> {
				textArea.setValue( "xxxxx" );
			}); */
		});
	}
	private void clearButton() {
		ToolStripButton button = AppUtil.createToolStripButton( "消去", IconUtil.icons.arrow_refresh() );
		addMember( button );
		button.addClickHandler( event-> {
			textArea.setValue( "" );
		} );
	}
	private void addTextValue( String value ) {
		String x = textArea.getValueAsString();
		x = value + "\n" + ZUtil.convNull( x );
		textArea.setValue( x );
	}
}
