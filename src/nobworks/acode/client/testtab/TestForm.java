package nobworks.acode.client.testtab;

import com.smartgwt.client.types.FormLayoutType;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;

import nobworks.acode.client.common.AppForm;
import nobworks.zsgwt.uclass.UTextAreaItem;

public class TestForm extends DynamicForm {

	private TextAreaItem textArea;
	public TestForm() {
//		setBorder( "1px solid red" );
		setWidth100();
		setHeight100();
		setItemLayout( FormLayoutType.ABSOLUTE );
		createUI();
	}
	public TextAreaItem getTextArea() {
		return textArea;
	}
	private void createUI() {
		textArea = new TextAreaItem();
		textArea.setShowTitle( false );
		textArea.setWidth( "100%" );
		textArea.setHeight( "100%" );
		textArea.setTop( 0 );
		textArea.setLeft( 0 );
		setFields( textArea );
	}

}
