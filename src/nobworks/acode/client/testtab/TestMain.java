package nobworks.acode.client.testtab;

import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.layout.VLayout;

public class TestMain extends VLayout {

	public TestMain() {
		createUI();
	}
	private void createUI() {
		setWidth100();
		setHeight100();
		
		TestForm form = new TestForm();
		TextAreaItem textAreaItem = form.getTextArea();

		TestToolStrip toolStrip = new TestToolStrip();
		toolStrip.setTextArea( textAreaItem );

		UrlConnectionToolStrip toolStrip2 = new UrlConnectionToolStrip();
		toolStrip2.setTextArea( textAreaItem );
		
		ZqlToolStrip toolStrip3 = new ZqlToolStrip();
		toolStrip3.setTextArea( textAreaItem );

		addMember( toolStrip );
		addMember( toolStrip2 );
		addMember( toolStrip3 );
		addMember( form );
	}
}
