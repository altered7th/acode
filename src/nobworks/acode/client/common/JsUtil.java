package nobworks.acode.client.common;

public class JsUtil {

	public static native void goURL( String url ) /*-{
		location.href = url;
	}-*/;
	public static native void setVisibility( String name, String value ) /*-{
		$wnd.document.getElementById( name ).style.visibility = value;
	}-*/;
	public static native void setDisplay( String name, String value ) /*-{
		$wnd.document.getElementById( name ).style.display = value;
	}-*/;
	public static native void openURL( String url ) /*-{
		window.open( url );
	}-*/;
	public static native void openURLwithOption( String url ) /*-{
		window.open( url, null, 'width=1200,height=900,top=100, left=200, toolbar=0, status=0' );
	}-*/;
	public static native void setTitle( String title ) /*-{
		$wnd.document.title = title;
	}-*/;
}
