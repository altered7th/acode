package nobworks.acode.client.common;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JsDate;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.Timer;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.fields.DataSourceIntegerField;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.events.KeyDownEvent;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

import nobworks.zcore.zclass.ZBrowserUtil;
import nobworks.zcore.zclass.ZListener;
import nobworks.zcore.zclass.ZObject;
import nobworks.zcore.zclass.ZUtil;
import nobworks.zcore.zclass.icons.IconUtil;
import nobworks.zsgwt.zclass.ZCanvasForm;

public class AppUtil {

	public static boolean isEnter( KeyDownEvent event ) {
		if ( event.getKeyName() == null ) {
			return false;
		}
		if ( event.getKeyName().equals( "Enter" ) ) {
			return true;
		} else {
			return false;
		}
	}
	public static void setSystemTime( SelectItem hourItem, SelectItem minuteItem, int addHour ) {
		JsDate date = JsDate.create();
		int hour = date.getHours() + addHour;
		if ( hour > 24 ) {
			hour -= 24;
		}
		int minute = date.getMinutes();
		int preM = -1;
		String minuteKey = "0";
		for ( String key : TimeValue.minutes.keySet() ) {
			int m = ZUtil.parseInt( key );
			if ( preM == -1 ) {
				preM = m;
				continue;
			}
			if ( preM <= minute && minute < m ) {
				minuteKey = key;
				break;
			}
		}
		hourItem.setValue( ZUtil.right( "0" + hour, 2 ) );
		minuteItem.setValue( TimeValue.minutes.get( minuteKey ) );
	}
	
	public static ToolStripButton createToolStripButton( String title, ImageResource icon ) {
		ToolStripButton button = new ToolStripButton( title, IconUtil.getURL( icon ) );
        button.setAutoFit( true );
        button.setShowDownIcon( false );
        button.setShowDisabledIcon( false );
        return button;	
	}
	public static ToolStripButton createToolStripButton( ImageResource icon ) {
		ToolStripButton button = new ToolStripButton();
		button.setIcon( IconUtil.getURL( icon ) );
        button.setAutoFit( true );
        button.setShowDownIcon( false );
        button.setShowDisabledIcon( false );
        return button;	
	}
	public static void dumpPara( ZObject para ) {
		Map buf = para.getMap();
		Set<String> names = buf.keySet();
		GWT.log( "------------" );
		for ( String name : names ) {
			GWT.log( "[ZObject] " + name + ":" + buf.get( name ) );
		}
	}
	public static void sleep( int ms, ZListener callback ) {
		Timer timer = new Timer() {
			@Override
			public void run() {
				callback.onSuccess( null );
			}
		};
		timer.schedule( ms );	
	}
	// LinkedHashMap を直接 SelectItem に setValueMap すると順番が変わるため、DataSource を使う。
	public final static String SELECT_PK = "pk";
	public final static String SELECT_TITLE = "title";
	public static String getSelectItemTitleValue( ZCanvasForm form, String formName ) {
		SelectItem selectItem = (SelectItem)form.getInputItem( formName ).getFormItem();
		ListGridRecord record = selectItem.getSelectedRecord();
		if ( record == null ) {
			return null;
		} else {
			return record.getAttributeAsString( SELECT_TITLE );
		}
	}
	public static String getSelectItemPkValue( ZCanvasForm form, String formName ) {
		ListGridRecord record = ((SelectItem)form.getInputItem( formName ).getFormItem()).getSelectedRecord();
		if ( record == null ) {
			return null;
		} else {
			return record.getAttributeAsString( SELECT_PK );
		}
	}
	public static void setValueMap( SelectItem selectItem, LinkedHashMap<String, String> valueMap ) {
		DataSource ds = new DataSource();
		ds.setID( "ds" + new Date().getTime() );
		DataSourceIntegerField pkField = new DataSourceIntegerField( SELECT_PK );
		pkField.setPrimaryKey( true );
		DataSourceTextField labelField = new DataSourceTextField( SELECT_TITLE );
		ds.setFields( pkField, labelField );
		ds.setClientOnly( true );

		selectItem.setOptionDataSource( ds );
		selectItem.setValueField( SELECT_PK );
		selectItem.setDisplayField( SELECT_TITLE );

		Set<String> keys = valueMap.keySet();
		for ( String key : keys ) {
			Record record = new Record();
			//				GWT.log( key + ":" + valueMap.get( key ) );
			record.setAttribute( SELECT_PK, key );
			record.setAttribute( SELECT_TITLE, valueMap.get( key ) );
			ds.addData( record );
		}
	}
	/*
	public static LogBean getLogBean( ZObject para ) {
		return (LogBean)para.get( "logBean" );
	}
	*/
	//-----------------------------------------------------------------------------------
	public static String getValue( ListGridRecord record, String name ) {
		return record.getAttributeAsString( name );
	}
	public static boolean isEmpty( ListGridRecord record, String name ) {
		return ZUtil.isEmpty( getValue( record, name ) );
	}
	//-----------------------------------------------------------------------------------
	public static ZObject createZObject( String className, String methodName,
		Class<?> clazz, String message ) {
		ZObject para = new ZObject( className, methodName );
		
		String clientClassName = clazz.getSimpleName();
		String programName = clientClassName + "->" + para.getClassName() + "#" + para.getMethodName();
//		String userId = Main.main.getUserId();
		String userAgent = ZBrowserUtil.getUserAgent();
//		LogBean logBean = new LogBean( userId, userAgent, "INFO", programName, message, detailInfo );
//		para.set( "logBean", logBean );
		
		return para;
	}
	//-----------------------------------------------------------------------------------
	public static String getTimeValue( Object value ) {
		DateTimeFormat dtf = DateTimeFormat.getFormat( "HH:mm" );
		String time = dtf.format( (Date)value );
		return time;
	}
	public static String getDateValue( Object value ) {
		DateTimeFormat dtf = DateTimeFormat.getFormat( "yyyy/MM/dd" );
		return dtf.format( (Date)value );
	}
	public static String getDateValue( Object value, String format ) {
		DateTimeFormat dtf = DateTimeFormat.getFormat( format );
		return dtf.format( (Date)value );
	}
	public static String getTimestamp() {
		DateTimeFormat dtf = DateTimeFormat.getFormat( "yyyy/MM/dd HH:mm:ss" );
		return dtf.format( new Date() );
	}
	public static String getTimestamp( Date date ) {
		DateTimeFormat dtf = DateTimeFormat.getFormat( "yyyy/MM/dd HH:mm:ss" );
		return dtf.format( date );
	}
	public static String getYyyymm( Date date ) {
    	String yyyy = ZUtil.getDateString( date ).substring( 0, 4 );
    	String mm = ZUtil.getDateString( date ).substring( 4, 6 );
		return yyyy + "/" + mm;
	}
	public static String getYyyymm6( Date date ) {
    	String yyyy = ZUtil.getDateString( date ).substring( 0, 4 );
    	String mm = ZUtil.getDateString( date ).substring( 4, 6 );
		return yyyy + mm;
	}
	public static String getYyyymmKanji( Date date ) {
    	String yyyy = ZUtil.getDateString( date ).substring( 0, 4 );
    	String mm = ZUtil.getDateString( date ).substring( 4, 6 );
		return yyyy + "年" + mm + "月";
	}
	public static int getHHMM( Date d ) {
		return d.getHours() * 100 + d.getMinutes();
	}
	public static Date createDate( int year, int month, int day, int hour, int minute ) {
		return new Date( year-1900, month-1, day, hour, minute );
	}
	public static Date createDate( int year, int month, int day ) {
		return new Date( year-1900, month-1, day, 0, 0 );
	}
	public static Date createDate( String yyyy_mm_dd_hh_mm ) {
		int year = ZUtil.parseInt( yyyy_mm_dd_hh_mm.substring( 0, 4 ) );
		int month = ZUtil.parseInt( yyyy_mm_dd_hh_mm.substring( 5, 5+2 ) );
		int day = ZUtil.parseInt( yyyy_mm_dd_hh_mm.substring( 8, 8+2 ) );
		int hour = ZUtil.parseInt( yyyy_mm_dd_hh_mm.substring( 11, 11+2 ) );
		int minute = ZUtil.parseInt( yyyy_mm_dd_hh_mm.substring( 14, 14+2 ) );
		return createDate( year, month, day, hour, minute );
	}
	public static Date createDate( Date date, int hhmm ) {
		Date newDate = new Date( date.getTime() );
		int hour = (int)(hhmm / 100);
		int minute = hhmm - hour*100;
		newDate.setHours( hour );
		newDate.setMinutes( minute );
		return newDate;
	}
	public static int getHHMM( int min ) {
		return min / 60 * 100 + min % 60;
	}
	public static String getHHMMString( int min ) {
		String hhmm = ZUtil.right( "000" + getHHMM( min ), 4 );
		return hhmm.substring( 0, 2 ) + ":" + hhmm.substring( 2, 4 );
	}
	public static String convHourMinuteString( int min ) {
		int hour = (int)(min / 60);
		int minute = min % 60;

		return hour + ":" + ZUtil.right( "0" + minute, 2 );
	}
	public static void addField( List<ListGridField> fieldList, String name, String title ) {
		ListGridField f;
		f = new ListGridField( name, title );
		fieldList.add( f );
	}
	public static void addField( List<ListGridField> fieldList, String name, String title, int width ) {
		ListGridField f;
		f = new ListGridField( name, title, width );
		fieldList.add( f );
	}
	public static ListGridField addField( List<ListGridField> fieldList, String name, String title, Alignment alignment ) {
		ListGridField f;
		f = new ListGridField( name, title );
		f.setAlign( alignment );
		fieldList.add( f );
		return f;
	}
	public static ListGridField addField( List<ListGridField> fieldList, String name, String title, Alignment alignment, int width ) {
		ListGridField f;
		f = new ListGridField( name, title, width );
		if ( title == null ) {
			f.setShowTitle( false );
		}
		f.setAlign( alignment );
		fieldList.add( f );
		return f;
	}
	public static ListGridField newField( String name, String title, int width ) {
		ListGridField field = new ListGridField( name, title );
		if ( width != 0 ) {
			field.setWidth( width );
		}
		return field;
	}
	@SuppressWarnings("deprecation")
	public static String getTimestampString( Date d ) {
		if ( d == null ) {
			return "";
		}
		String s = ( d.getYear() + 1900 )
			+ "/"
			+ ZUtil.right( "0" + ( d.getMonth() + 1 ), 2 )
			+ "/"
			+ ZUtil.right( "0" + d.getDate(), 2 )
			+ " "
			+ ZUtil.right( "0" + d.getHours(), 2 )
			+ ":"
			+ ZUtil.right( "0" + d.getMinutes(), 2 )
			;
		return s;
	}
	@SuppressWarnings("deprecation")
	public static String getHHMMString( Date d ) {
		if ( d == null ) {
			return "";
		}
		String s =
			ZUtil.right( "0" + d.getHours(), 2 )
			+ ":"
			+ ZUtil.right( "0" + d.getMinutes(), 2 )
			;
		return s;
	}
	public static native void goURL( String url ) /*-{
		location.href = url;
	}-*/;
}
