package nobworks.acode.client.common;

import com.google.gwt.resources.client.ImageResource;
import com.smartgwt.client.types.Side;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;

import nobworks.zcore.zclass.icons.IconUtil;

public class AppTabSet extends TabSet {

	public AppTabSet() {
		setTabBarPosition( Side.TOP );
		setWidth100();
		setHeight100();
	}
	public Tab addTab( String title, ImageResource icon, Canvas app ) {
		Tab tab = new Tab( title, IconUtil.getURL( icon )  );
		if ( app != null ) {
			tab.setPane( app );
		}
		addTab( tab );
		return tab;
	}
}
