package nobworks.acode.client.common;

import java.util.ArrayDeque;
import java.util.Deque;

import nobworks.zcore.zclass.ZListener;

public class AsyncQue {

	private Deque<String> que;
	private ZListener finishCallback;

	public AsyncQue( ZListener finishCallback, String ... tasks ) {
		this.finishCallback = finishCallback;
		que = new ArrayDeque<>();
		for ( String task : tasks ) {
			que.push( task );
		}
	}
	public void done() {
		que.pop();
		if ( que.size() == 0 ) {
			finishCallback.onSuccess( null );
		}
	}
}
