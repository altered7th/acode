package nobworks.acode.client.common;

import com.smartgwt.client.widgets.form.fields.LinkItem;

import nobworks.zsgwt.zclass.ZInputItem;

/**
 * 
 * nobworks.zsgwt.uclass に昇格予定(2021/07/23)
 *
 */
public class ULinkItem extends ZInputItem {

	public ULinkItem( String name, String title, int x, int y ) {
		super( title, new LinkItem() );
		setName( name );
		setTop( y );
		setLeft( x );
		if ( title == null ) {
			getItem().setShowTitle( false );
		}
	}
	public ULinkItem( String name, String title, int x, int y, int titleWidth ) {
		super( title, new LinkItem() );
		setName( name );
		setTop( y );
		setLeft( x );
		setTitleWidth( titleWidth );
		if ( title == null ) {
			getItem().setShowTitle( false );
		}
	}
	public LinkItem getItem() {
		return (LinkItem)formItem;
	}
}
