package nobworks.acode.client.common;

import java.util.LinkedHashMap;

import nobworks.zcore.zclass.ZUtil;

public class TimeValue {

	public static LinkedHashMap<String, String> hours = new LinkedHashMap<String, String>();
	public static LinkedHashMap<String, String> minutes = new LinkedHashMap<String, String>();
	static {
		for ( int i=0;i<24;i++ ) {
			String hour = ZUtil.right( "0" + i, 2 );
			hours.put( hour, hour );
		}
		
		minutes.put( "00", "00" );
		minutes.put( "05", "05" );
		minutes.put( "10", "10" );
		minutes.put( "15", "15" );
		minutes.put( "20", "20" );
		minutes.put( "25", "25" );
		minutes.put( "30", "30" );
		minutes.put( "35", "35" );
		minutes.put( "40", "40" );
		minutes.put( "45", "45" );
		minutes.put( "50", "50" );
		minutes.put( "55", "55" );
	}
}
