package nobworks.acode.client.common;

import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ImageResource;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.TimeItem;

import nobworks.zcore.zclass.ZUtil;
import nobworks.zcore.zclass.icons.IconUtil;

public class WSUtil {

	public static IButton newButton( String title ) {
		return newButton( title, 60 );
	}
	public static IButton newButton( String title, int width ) {
		IButton button = new IButton( title );
		button.setWidth( width );
		button.setShowDisabledIcon( false );
		return button;
	}
	public static IButton newButton( String title, int width, String icon ) {
		IButton button = newButton( title, width );
		button.setIcon( icon );
		return button;
	}

	public static String getSimpleTime( int min ) {
		int hour = (int) ( min / 60 );
		int minute = min % 60;
		return ZUtil.right( "0" + hour, 2 ) + ":" + ZUtil.right( "0" + minute, 2 );
	}
	public static String conv00( int value ) {
		return ZUtil.right( "0" + value, 2 );
	}

	public static Canvas newSpace( int height, int width ) {
		Canvas space = new Canvas();
		space.setHeight( height );
		space.setWidth( width );
		return space;
	}

	public static Canvas newSpaceBorder( int height, int width ) {
		Canvas space = new Canvas();
		space.setHeight( height );
		space.setWidth( width );
		space.setBorder( "1px solid lightgray" );
		return space;
	}

	public static Canvas newSpace( int height ) {
		Canvas space = new Canvas();
		space.setHeight( height );
		space.setWidth100();
		return space;
	}

	public static Canvas newSpaceBorder( int height ) {
		Canvas space = new Canvas();
		space.setHeight( height );
		space.setWidth100();
		space.setBorder( "1px solid lightgray" );
		return space;
	}

	//------------------------------------------------------------------
	public static ImgButton createImgButton( ImageResource icon ) {
		ImgButton button = new ImgButton();
		button.setWidth( 18 );
		button.setHeight( 18 );
		button.setShowImageDown( false );
		button.setShowImageRollOver( false );
		button.setSrc( IconUtil.getURL( icon ) );
		return button;
	}
	public static TimeItem createTimeItem( String title ) {
		TimeItem timeItem = new TimeItem();
		timeItem.setTitle( title );
		timeItem.setUseTextField( false );
		timeItem.setUse24HourTime( true );
		timeItem.setShowSecondItem( false );	
		timeItem.setWrapTitle( false );
		
		return timeItem;
	}
}
