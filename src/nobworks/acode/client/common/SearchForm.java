package nobworks.acode.client.common;

import com.google.gwt.core.client.GWT;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.TextItem;

import nobworks.zcore.zclass.ZListener;

public class SearchForm extends AppForm {
	
	private ZListener enterCallback;
	private TextItem searchItem;
	
	public SearchForm() {
		super( "SearchForm", 40, 24 );
		setHeight( 30 );
		setWidth100();
		createUI();
		createHandler();
	}
	public String getValue() {
		return searchItem.getValueAsString();
	}
	public void addEnterKeyHandler( ZListener enterCallback ) {
		this.enterCallback = enterCallback;
	}
	private void createHandler() {
		searchItem = (TextItem)getInputItem( "search" ).getFormItem();
		searchItem.addKeyDownHandler( event-> {
			if ( isEnter( event ) ) {
				enterCallback.onSuccess( searchItem.getValue() );
			}
		});
	}
	private void createUI() {
		add( createTextItem( "search", "検索：", 0, 2, 200 ) );
		prepare();
	}
}
