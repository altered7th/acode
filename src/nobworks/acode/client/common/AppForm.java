package nobworks.acode.client.common;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.RadioGroupItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.grid.ListGridRecord;

import nobworks.zcore.zclass.ZUtil;
import nobworks.zsgwt.uclass.UComboBoxItem;
import nobworks.zsgwt.uclass.UDateItem;
import nobworks.zsgwt.uclass.UDateTimeItem;
import nobworks.zsgwt.uclass.URadioGroupItem;
import nobworks.zsgwt.uclass.USelectItem;
import nobworks.zsgwt.uclass.USpinnerItem;
import nobworks.zsgwt.uclass.UStaticTextItem;
import nobworks.zsgwt.uclass.UTextAreaItem;
import nobworks.zsgwt.uclass.UTextItem;
import nobworks.zsgwt.uclass.UTimeItem;
import nobworks.zsgwt.zclass.ZCanvasForm;

public class AppForm extends ZCanvasForm {
	
	protected static final int DEFAULT_TITLE_WIDTH = 120;
	public static final int INPUT_HEIGHT = 28;
	private static int DATE_WIDTH = 110;
	private static final int DATETIME_WIDTH = 200;
	public static int STATIC_ITEM_ADJUST_Y = -5;

	public AppForm( String formName, int width, int height  ) {
		super( formName );
		setStaticItemAdjustY( STATIC_ITEM_ADJUST_Y );
		setAbsolute( width, height );
//		setWidth( width );
//		setHeight( height );
	}
	public AppForm( String formName, int height  ) {
		super( formName );
		setStaticItemAdjustY( STATIC_ITEM_ADJUST_Y );
		setAbsolute( DEFAULT_TITLE_WIDTH, INPUT_HEIGHT );
		setWidth100();
		setHeight( height );
	}
	public AppForm( String formName ) {
		super( formName );
		setStaticItemAdjustY( STATIC_ITEM_ADJUST_Y );
		setAbsolute( DEFAULT_TITLE_WIDTH, INPUT_HEIGHT );
		setWidth100();
		setHeight100();
	}
	public void setDefaultDateWidth( int dateWidth ) {
		DATE_WIDTH = dateWidth;
	}

	protected UTextItem createTextItem( String name, int x, int y, int width ) {
		UTextItem item = new UTextItem( name, x, y );
		item.setWidth( width );
		return item;
	}
	protected UTextItem createTextItem( String name, String title, int x, int y, int width ) {
		UTextItem item = new UTextItem( name, title, x, y );
//		item.setAlign( Alignment.RIGHT );
		item.setWidth( width );
//		item.setValue( value );
		return item;
	}
	protected UTextItem createTextItem( String name, String title, int x, int y, int width, ListGridRecord record ) {
		UTextItem item = createTextItem( name, title, x, y, width );
		if ( record != null ) {
			item.setValue( record.getAttributeAsString( name ) );
		}
		return item;
	}
	//------------------------------------------------
	protected UDateItem createDateItem( String name, int x, int y ) {
		UDateItem item = new UDateItem( name, null, x, y, true );
		item.setAlign( Alignment.CENTER );
		item.setWidth( DATE_WIDTH );
		return item;
	}
	protected UDateItem createDateItem( String name, int x, int y, boolean simple ) {
		UDateItem item = new UDateItem( name, null, x, y );
		item.setAlign( Alignment.CENTER );
		item.setWidth( DATE_WIDTH );
		return item;
	}
	protected UDateItem createDateItem( String name, String title, int x, int y ) {
		UDateItem item = new UDateItem( name, title, x, y, true );
		item.setAlign( Alignment.CENTER );
		item.setWidth( DATE_WIDTH );
		return item;
	}
	protected UDateItem createDateItem( String name, String title, int x, int y, int titleWidth ) {
		UDateItem item = new UDateItem( name, title, x, y, true );
		item.setAlign( Alignment.CENTER );
		item.setWidth( DATE_WIDTH );
		item.setTitleWidth( titleWidth );
		return item;
	}
	protected UDateItem createDateItem( String name, String title, int x, int y, boolean simple ) {
		UDateItem item = new UDateItem( name, title, x, y );
		item.setAlign( Alignment.CENTER );
		item.setWidth( DATE_WIDTH );
		return item;
	}
	protected UDateItem createDateItem( String name, String title, int x, int y, Date value ) {
		UDateItem item = createDateItem( name, title, x, y );
		item.setValue( value );
		return item;
	}
	//------------------------------------------------
	protected UDateTimeItem createDateTimeItem( String name, int x, int y ) {
		UDateTimeItem item = new UDateTimeItem( name, null, x, y, true );
		item.setAlign( Alignment.CENTER );
		item.setWidth( DATETIME_WIDTH );
		return item;
	}
	protected UDateTimeItem createDateTimeItem( String name, String title, int x, int y ) {
		UDateTimeItem item = new UDateTimeItem( name, title, x, y, true );
		item.setAlign( Alignment.CENTER );
		item.setWidth( DATETIME_WIDTH );
		return item;
	}
	//------------------------------------------------
	protected UTimeItem createTimeItem( String name, String title, int x, int y ) {
		UTimeItem item = new UTimeItem( name, title, x, y );
		
		return item;
	}
	//------------------------------------------------
	protected UStaticTextItem createStaticTextItem( String name, String title, int x, int y, String value, int valueWidth ) {
		return createStaticTextItem( name, title, x, y, value, valueWidth, 0 );
	}
	protected UStaticTextItem createStaticTextItem( String name, String title, int x, int y, int valueWidth ) {
		return createStaticTextItem( name, title, x, y, "", valueWidth, 0 );
	}
	protected UStaticTextItem createStaticTextItem( String name, int x, int y, int valueWidth ) {
		return createStaticTextItem( name, null, x, y, "", valueWidth, 0 );
	}
	protected UStaticTextItem createStaticTextItem( String name, String title, int x, int y, int valueWidth, int titleWidth ) {
		return createStaticTextItem( name, title, x, y, "", valueWidth, titleWidth );
	}
	protected UStaticTextItem createStaticTextItem( String name, String title, int x, int y, ListGridRecord record, int valueWidth ) {
		String value = "";
		if ( record != null ) {
			value = record.getAttributeAsString( name );
		}
		return createStaticTextItem( name, title, x, y, value, valueWidth, 0 );
	}
	protected UStaticTextItem createStaticTextItem( String name, String title, int x, int y, String value, int valueWidth, int titleWidth ) {
		UStaticTextItem item = new UStaticTextItem( name, title, x, y, titleWidth );
		item.setWidth( valueWidth );
		item.setValue( value );
		return item;
	}
	//------------------------------------------------
	protected UTextAreaItem createTextAreaItem( String name, String title, int x, int y, int width, int height ) {
		UTextAreaItem item = new UTextAreaItem( name, title, x, y, width, height );
		//item.setValue( "XXXXXX\nYYYY" );
		return item;
	}
	//------------------------------------------------
	protected URadioGroupItem createRadioGroupItem( String name, String title, int x, int y, int titleWidth ) {
		URadioGroupItem uItem = new URadioGroupItem( name, title, x, y, titleWidth );
		RadioGroupItem item = uItem.getItem();
		item.setVertical( false );
		item.setValueMap( "あり", "なし" );
		item.setValue( "あり" );
		return uItem;
	}
	protected URadioGroupItem createRadioGroupItem( String name, String title, int x, int y, int titleWidth, String[] values ) {
		URadioGroupItem uItem = new URadioGroupItem( name, title, x, y, titleWidth );
		RadioGroupItem item = uItem.getItem();
		item.setVertical( false );
		item.setValueMap( values );
		item.setValue( values[0] );
		return uItem;
	}
	//------------------------------------------------
	protected USelectItem createSelectItem( String name, String title, int x, int y, int width ) {
		USelectItem uItem = new USelectItem( name, title, x, y );
		SelectItem item = uItem.getItem();
		item.setWidth( width );
		return uItem;
	}
	protected USelectItem createSelectItem( String name, String title, int x, int y, int width, int titleWidth ) {
		USelectItem uItem = new USelectItem( name, title, x, y, titleWidth );
		SelectItem item = uItem.getItem();
		item.setWidth( width );
		return uItem;
	}
	protected USelectItem createSelectItem( String name, String title, int x, int y, int width,
		ListGridRecord record, String ...values ) {
		USelectItem item = createSelectItem( name, title, x, y, width );
		
		LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
		for ( String value : values ) {
			valueMap.put( value, value );
		}
		SelectItem selectItem = (SelectItem)item.getFormItem();
		AppUtil.setValueMap( selectItem, valueMap );
		
		if ( record == null ) {
			selectItem.setValue( values[0] );
		} else  {
			String recordValue = record.getAttributeAsString( name );
			if ( recordValue != null ) {
				if ( valueMap.get( recordValue ) != null ) {
					selectItem.setValue( recordValue );
				}
			}
		}
		return item;
	}
	//------------------------------------------------
	protected USpinnerItem createSpinnerItem( String name, String title, int x, int y, int titleWidth ) {
		USpinnerItem uItem = new USpinnerItem( name, title, x, y, titleWidth );
		return uItem;
	}
	protected USpinnerItem createSpinnerItem( String name, String title, int x, int y ) {
		USpinnerItem uItem = new USpinnerItem( name, title, x, y );
		return uItem;
	}
	//------------------------------------------------
	protected ULinkItem createLinkItem( String name, String title, int x, int y ) {
		ULinkItem item = new ULinkItem( name, title, x, y );
		return item;
	}
	protected ULinkItem createLinkItem( String name, String title, int x, int y, int titleWidth ) {
		ULinkItem item = new ULinkItem( name, title, x, y, titleWidth );
		return item;
	}
	protected ULinkItem createLinkItem( String name, String title, int x, int y, String value, int valueWidth, int titleWidth ) {
		ULinkItem item = new ULinkItem( name, title, x, y, titleWidth );
		item.setWidth( valueWidth );
		item.setValue( value );
		return item;
	}
	//------------------------------------------------
	protected UComboBoxItem createComboBoxItem( String name, String title, int x, int y, int width ) {
		UComboBoxItem uItem = new UComboBoxItem( name, title, x, y );
		ComboBoxItem item = uItem.getItem();
		item.setWidth( width );
		
		return uItem;
	}
	//------------------------------------------------
	protected void setButtonIcon( String name, String icon ) {
		ButtonItem button = (ButtonItem)getInputItem( name ).getFormItem();
		button.setIcon( icon );
	}
	protected void setRecordToFormItem( String name, ListGridRecord record ) {
		getInputItem( name ).setValue( record.getAttributeAsString( name ) );
	}
	protected double getDouble( String name ) {
		return ZUtil.parseDouble( getInputItem( name ).getValueAsString() );
	}
	public SelectItem getSelectItem( String itemName ) {
		return (SelectItem)getInputItem( itemName ).getFormItem();
	}
	public String getSelectItemTitle( String itemName ) {
		if ( getInputItem( itemName ) == null ) {
			GWT.log( "AppForm.getSelectItemTitle():itemName=" + itemName + " not found" );
			return null;
		}
		SelectItem item = getSelectItem( itemName );
		ListGridRecord record = item.getSelectedRecord();
		if ( record == null ) {
			return "";
		}
		return ZUtil.convNull( record.getAttributeAsString( "title" ) );
	}
	//------------------------------------------------
	// 2021/07/20 変数名を使わないで値を一時保持する
	private Map<String,String> valueMap = null;
	public void saveValue( String name, String value ) {
		if ( valueMap == null ) {
			valueMap = new HashMap<String,String>();
		}
		valueMap.put( name, value );
	}
	public String loadValue( String name ) {
		if ( valueMap == null ) {
			valueMap = new HashMap<String,String>();
		}
		return ZUtil.convNull( valueMap.get( name ) );
	}
}