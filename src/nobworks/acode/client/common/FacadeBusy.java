package nobworks.acode.client.common;

import nobworks.zsgwt.zclass.ZBusyLight;

public class FacadeBusy {
	
	private static boolean startBusy = false;

	public static void on() {
		on( "処理中" );
	}
	public static void on( String message ) {
		startBusy = true;
		// 時限起動
		// App Engine インスタンスのスピンアップ
		// 300ms 以下の短時間処理はビジーにしない
		AppUtil.sleep( 300, callback-> {
			if ( startBusy ) {
				ZBusyLight.on( message );
			}
		});
	}
	public static void off() {
		startBusy = false;
		ZBusyLight.off();
	}
}
