package nobworks.acode.client.common;

public class ColorDefine {

	// 全体の背景色
	public final static String AppBaseLayout_BGCOLOR = "#f9fcff";
	
	// 時間軸の背景色
	public final static String HourLabel_EVEN_BGCOLOR = "#e5f2ff";		// "#e6e6fa";
	public final static String HourLabel_ODD_BGCOLOR = "#eff7ff";		// "#efefff";
	// 時間軸左側スペースの背景色
	public final static String HoursLeftSpace_BGCOLOR = "#f9fcff";
	
	// 必要人数バーの背景色（右）
	public final static String RequiredNumberBar_BGCOLOR = "#dbedff";		// "#f9fffc";
	// 必要人数タイトル部の背景色
	public final static String RequiredNumberTitle_BGCOLOR = "#d6eaff";		// "#e6e6fa";
	
	// 必要人数バー
	public final static String StaffingColorBar_patter1 = "black";
	public final static String StaffingColorBar_patter2 = "purple";
	public final static String StaffingColorBar_patter3 = "lightgray";
	public final static String StaffingColorBar_patter4 = "deeppink";
	public final static String StaffingColorBar_patter5 = "pink";
	public final static String StaffingColorBar_patter6 = "yellow";
	public final static String StaffingColorBar_patter7 = "greenyellow";
	public final static String StaffingColorBar_patter8 = "#1e90ff";
	
	// スタッフ氏名の背景色
	public final static String StaffTaskLayout_Name_EVEN_BGCOLOR = "#efefff";
	public final static String StaffTaskLayout_Name_ODD_BGCOLOR = "#eaeaff";
	// スタッフタスクの背景色
	public final static String StaffTaskLayout_Task_EVEN_BGCOLOR = "#f5f5ff";
	public final static String StaffTaskLayout_Task_ODD_BGCOLOR = "#f8f8ff";
	
}
