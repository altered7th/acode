package nobworks.acode.client.common;

import com.google.gwt.core.client.GWT;
import com.smartgwt.client.util.SC;

import nobworks.acode.shared.Release;
import nobworks.zcore.zclass.ZListener;

public class GoogleApi {

	public static native void loadGapi() /*-{
		$wnd.gapi.client.load( 'gmail', 'v1' );
	}-*/;

	public static native void setGapiAccessToken( String accessToken ) /*-{
		$wnd.gapi.auth.setToken({
			access_token: accessToken
		});
	}-*/;
	
	public static void sendMail( String to, String subject, String message ) {
		if ( true ) {
			sendMailNative( to, subject, message );
		} else {
			SC.say( "ダミーメール", "送信( " + to + "," + subject + ")" );
		}
	}
	public static void mailListener( ZListener listener ) {
		mailListener = listener;
	}
	private static ZListener mailListener;
	public static void mailCallback() {
		mailListener.onSuccess( null );
	}
	public static native void sendMailNative( String to, String subject, String message ) /*-{
		var mimeData = ["To: " + to,
		"Subject: =?utf-8?B?" + window.btoa(unescape(encodeURIComponent( subject ))) + "?=",
		"MIME-Version: 1.0",
		"Content-Type: text/plain; charset=UTF-8",
		"Content-Transfer-Encoding: 7bit",
		"",
		message ].join("\n").trim();
	
		var raw = window.btoa(unescape(encodeURIComponent(mimeData))).replace(/\+/g, '-').replace(/\//g, '_');
		
//		console.log( "gapi=" + $wnd.gapi );
		$wnd.gapi.client.gmail.users.messages.send({
			'userId': 'me',
			'resource': {
				'raw': raw
			}
		}).execute(function() {
			// 送信後の処理
//			console.log( "メール送信終了:" + to );
			@nobworks.evidex.client.common.GoogleApi::mailCallback()();
		});
	}-*/;
	
	public static void testMail() {
		String to = "kiku@nobworks.co.jp";
		String subject = "タイトルテスト";
		String message = "これはテスト。";
		sendMail( to, subject, message );
	}
}
