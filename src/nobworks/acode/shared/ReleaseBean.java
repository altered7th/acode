package nobworks.acode.shared;

public class ReleaseBean {

	private String key;
	private String clientId;
	private String name;

	public ReleaseBean() {}
	public ReleaseBean( String key, String clientId, String name, String udl ) {
		this.key = key;
		this.clientId = clientId;
		this.name = name;
		// this.url = url;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
