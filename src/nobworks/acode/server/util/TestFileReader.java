package nobworks.acode.server.util;

import java.util.Scanner;

public class TestFileReader {
	
	private String folder;
	
	// src/ 以下のフォルダを 「/」から指定する
	// Ex) "/nobworks/acode/server/graphql/test1/"
	public TestFileReader( String folder ) {
		this.folder = folder;
	}

	public String getContents( String fileName ) {
		Scanner scanner = new Scanner( TestFileReader.class.getResourceAsStream( folder + fileName ), "UTF-8" );
		StringBuilder result = new StringBuilder( "" );
		while( scanner.hasNextLine() ) {
			String line = scanner.nextLine();
			result.append( line ).append( "\n" );
		}
		String contents = result.toString();
		scanner.close();
		
		return new String( contents );
	}
	public String getContentsNoCr( String fileName ) {
		Scanner scanner = new Scanner( TestFileReader.class.getResourceAsStream( folder + fileName ), "UTF-8" );
		StringBuilder result = new StringBuilder( "" );
		while( scanner.hasNextLine() ) {
			String line = scanner.nextLine();
			result.append( line );
		}
		String contents = result.toString();
		scanner.close();
		
		return new String( contents );
	}
}
