package nobworks.acode.server.util;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;

import nobworks.zcore.zclass.ZUtil;

public class HeapUtil {

	public static String getHeap() {
		ThreadMXBean thread = (java.lang.management.ThreadMXBean)ManagementFactory.getThreadMXBean();
//		System.out.println( "threadcount=" + thread.getThreadCount() );
		
		int threadCount = thread.getThreadCount();
		
		Runtime runtime = Runtime.getRuntime();
		long totalMemory = runtime.totalMemory();
        long freeMemory = runtime.freeMemory();
        long usedMemory = totalMemory - freeMemory;
        double usedRate = ( (double)usedMemory / (double)totalMemory ) * 100;

        /*
        System.out.println( "totalMemory=" + megaBytes( totalMemory ) );
        System.out.println( "freeMemory =" + megaBytes( freeMemory ) );
        System.out.println( "usedMemory =" + megaBytes( usedMemory ) );
        System.out.println( "usedRate   =" + ZUtil.adjustDouble( usedRate, 2 ) + "%" );
        */
        String s =
        	"used%:" + ZUtil.adjustDouble( usedRate, 2 ) + "%"
        	+ "," + "free:" + megaBytes( freeMemory )
        	+ "," + "used:" + megaBytes( usedMemory ) 
        	+ "," + "total:" + megaBytes( totalMemory )
        	+ "," + "thread:" + threadCount
        	;
        return s;
	}
	private static String megaBytes( long value ) {
		return (int)(value/1024/1024) + "MB";
	}
}
