package nobworks.acode.server.graphql;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import graphql.kickstart.tools.GraphQLResolver;
import nobworks.acode.server.ContentsFactory;

/*
 * REST parameter
 * @param query GraphQL query
 * @param input GraphQL input
 */
public class ZGraphQLServlet extends HttpServlet {
	
	protected ZGraphQL graphql;
	
	public ZGraphQLServlet( String schema, GraphQLResolver<?> resolver ) {
		graphql = new ZGraphQL( schema, resolver );
	}

	public void doGet( HttpServletRequest req , HttpServletResponse res ) throws IOException,ServletException {
		out( res, "" );
		
	}
	public void doPost( HttpServletRequest req , HttpServletResponse res ) throws IOException,ServletException {
		String query = req.getParameter( "query" );
		String input = req.getParameter( "input" );
		
		String json = graphql.runGql( query, input );
		
		out( res, json );
	}
	
	private void out( HttpServletResponse res, String contents ) throws IOException,ServletException {
		res.setContentType( "text/html; charset=UTF-8" );
		PrintWriter out = res.getWriter();
		out.println( contents );
	}
}
