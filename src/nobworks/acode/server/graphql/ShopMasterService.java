package nobworks.acode.server.graphql;

import java.sql.Connection;

import javax.servlet.ServletContext;

import nobworks.acode.server.graphql.sample1.ShopSample;
import nobworks.zcore.zclass.ZObject;
import test.hello.HelloTools;

public class ShopMasterService {

	public ZObject testH2Connection( ZObject para ) {
		
		return para;
	}

	public ZObject getAllShops( ZObject para ) {
		ShopSample shopSample = new ShopSample();
		String json = shopSample.getAllShops();
		para.set( "json", json );
		
		return para;
	}
	
	public ZObject getShop( ZObject para ) {
		ShopSample shopSample = new ShopSample();
		String shopNo = para.getString( "shopNo" );
		String json = shopSample.getShop( shopNo );
		para.set( "json", json );
		
		return para;
	}
	
	public ZObject writeShop( ZObject para ) {
		ShopSample shopSample = new ShopSample();
		String json = shopSample.writeShop( "writeShop1.gql" );
		para.set( "json", json );
		
		return para;
	}
	public ZObject writeShops( ZObject para ) {
		ShopSample shopSample = new ShopSample();
		String json = shopSample.writeShops();
		para.set( "json", json );
		
		return para;
	}
	
	//---------------------------------------------------------------------

	public ZObject writeShopServlet( ZObject para ) {
		
		String scriptFile = para.getString( "scriptFile" );
		ShopSample shopSample = new ShopSample();
		String json = shopSample.writeShopServlet( scriptFile );
		para.set( "json", json );
		
		return para;
	}
	
	public ZObject urlGetShop( ZObject para ) {
		ShopSample shopSample = new ShopSample();
		String shopNo = para.getString( "shopNo" );
		String json = shopSample.getShopServlet( shopNo );
		para.set( "json", json );
		
		return para;
	}
	
	public ZObject urlGetAllShops( ZObject para ) {
		ShopSample shopSample = new ShopSample();
		String json = shopSample.getAllShopsServlet();
		para.set( "json", json );
		
		return para;
	}
	
	public ZObject urlWriteShops( ZObject para ) {
		ShopSample shopSample = new ShopSample();
		String json = shopSample.writeShopsServlet();
		para.set( "json", json );
		
		return para;
	}
}
