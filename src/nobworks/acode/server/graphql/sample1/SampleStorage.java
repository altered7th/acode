package nobworks.acode.server.graphql.sample1;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

// mutation データを一時的にキャッシュする
public class SampleStorage {
	
	private static Map<String,ShopMaster> shopMap = new LinkedHashMap<String, ShopMaster>();
	private static Map<String,Document> documentMap = new LinkedHashMap<String, Document>();

	public static void setShops( ShopMaster[] shopList ) {
		for ( ShopMaster shop : shopList ) {
			shopMap.put( shop.getShopNo(), shop );
		}
	}
	public static void setShop( ShopMaster shop ) {
		shopMap.put( shop.getShopNo(), shop );
	}
	public static ShopMaster getShop( String shopNo ) {
		return shopMap.get( shopNo );
	}
	
	public static List<ShopMaster> getShopList() {
		return new ArrayList<ShopMaster>( shopMap.values() );
	}
	
	public static void setDocument( String docNo, Document csv ) {
		documentMap.put( docNo, csv );
	}
	public static Document getDocument( String docNo ) {
		return documentMap.get( docNo );
	}
}
