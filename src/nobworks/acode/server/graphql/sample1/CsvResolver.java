package nobworks.acode.server.graphql.sample1;

import graphql.kickstart.tools.GraphQLMutationResolver;
import graphql.kickstart.tools.GraphQLQueryResolver;

public class CsvResolver implements GraphQLQueryResolver, GraphQLMutationResolver {

	public Document getCsv( String docNo ) {
		Document doc = SampleStorage.getDocument( docNo );
		return doc;
	}
	public Document setCsv( String docNo, String csv ) {
//		System.out.println( "CsvResolver:csv=" + csv );
		// csv の改行コードは mutation のスクリプトではエスケープされているが
		// ここでは改行された状態で渡される
		Document doc = new Document( docNo, csv );
		SampleStorage.setDocument( docNo, doc );
		return doc;
	}
}
