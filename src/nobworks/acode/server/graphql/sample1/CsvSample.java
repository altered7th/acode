package nobworks.acode.server.graphql.sample1;

import nobworks.acode.server.graphql.ZGraphQL;
import nobworks.acode.server.util.TestFileReader;

public class CsvSample extends ZGraphQL {

	private TestFileReader reader;

	public CsvSample() {
		super( "schema/csv.graphqls", new CsvResolver() );
		reader = new TestFileReader( "/nobworks/acode/server/graphql/sample1/" );
	}
	public void run() {
		writeDocument();
	}
	private void writeDocument() {
		String query = reader.getContents( "setCsv.gql" );
		String csv = reader.getContentsNoCr( "csvSample.csv" );
//		System.out.println( csv + "----" );
		
		query = query.replace( "%docNo%", "doc001" ).replace( "%csv%", csv );
//		System.out.println( query );
		String json = runGql( query );
		System.out.println( "writeDocument() json=" + json );
	}
}
