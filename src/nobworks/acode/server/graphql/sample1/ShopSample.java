package nobworks.acode.server.graphql.sample1;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import nobworks.acode.server.graphql.ZGraphQL;
import nobworks.acode.server.util.TestFileReader;

/**
 * 店舗マスタの一括登録、1件登録、全取得、検索取得のサンプル
 */
public class ShopSample extends ZGraphQL {

	private TestFileReader reader;
	
	public ShopSample() {
		super( "schema/shopmaster.graphqls", new ShopResolver() );
		reader = new TestFileReader( "/nobworks/acode/server/graphql/sample1/" );
	}
	public void run() {
//		writeShopsServlet( "writeShops.gql", "writeShopsVariables.gql" );
//		writeShopsServlet( "writeShops.gql", "bigData.gql" );
//		writeShopServlet( "writeShop1.gql" );
//		writeShopServlet( "writeShop3.gql" );
//		getShopServlet( "shop-003" );

//		writeShop( "writeShop1.gql" );
//		writeShop( "writeShop3.gql" );
		writeShops();
		getShop( "shop-00x" );
//		getShop( "shop-003" );
//		getShop( "shop-004" );
//		getShop( "shop-500" );
//		getAllShops();
	}
	public String writeShop( String scriptFile ) {
		String query = reader.getContents( scriptFile );
		String json = runGql( query );
//		System.out.println( "writeShop() json=" + json );
		return json;
	}
	public String writeShops() {
		String query = reader.getContents( "writeShops.gql" );
		String variables = reader.getContents( "writeShopsVariables.gql" );
		
		// 大量データの初回は比較的時間がかかるが、2回目移行は数分の1程度になる
//		String variables = reader.getContents( "bigData.gql" );

		String json = runGql( query, variables );	
//		System.out.println( "writeShops() json=" + json.length() );
		
		// ShopResolver で SampleStorage に保管されている
		List<ShopMaster> shopList = SampleStorage.getShopList();
//		System.out.println( "\tshopList=" + shopList.size() );
		/*
		for ( Object obj : shopList ) {
			if ( obj instanceof ShopMaster ) {
				ShopMaster shop = (ShopMaster)obj;
				System.out.println( "\tshopNo=" + shop.getShopNo() );
				for ( Staff staff : shop.getStaffList() ) {
					System.out.println( "\t\t" + staff.getStaffName() );
				}
			}
		}
		*/
		
//		System.out.println( json );	// NG:大量データのコンソール表示
//		System.out.println( "end of writeShops():" + json.length() );
		return json;
	}
	public String getShop( String shopNo ) {
		String query = reader.getContents( "getShop.gql" ).replace( "%shopNo%", shopNo );
		System.out.println( query );
		String json = runGql( query );
		System.out.println( "getShop() json=" + json );
		return json;
	}
	public String getAllShops() {
		String query = reader.getContents( "getAllShops.gql" );
		String json = runGql( query );
//		System.out.println( "getShop() json=" + json );
		return json;
	}
	//---------------------------------------------------------------------------
	// Servlet test
	//---------------------------------------------------------------------------
	public String writeShopsServlet() {
		String query = reader.getContents( "writeShops.gql" );
		String variables = reader.getContents( "writeShopsVariables.gql" );
		String json = runGql( query, variables );	
		return json;
	}
	public String getAllShopsServlet() {
		String query = reader.getContents( "getAllShops.gql" );
		String json = callServlet( query, null );
		return json;	
	}
	public String writeShopServlet( String scriptFile ) {
		String query = reader.getContents( scriptFile );
		String json = callServlet( query, null );
//		System.out.println( json );
		return json;
	}
	public String getShopServlet( String shopNo ) {
		String query = reader.getContents( "getShop.gql" ).replace( "%shopNo%", shopNo );
		String json = callServlet( query, null );
//		System.out.println( json );
		return json;
	}
	public String writeShopsServlet( String scriptFile, String inputFile ) {
		String query = reader.getContents( scriptFile );
		String input = reader.getContents( inputFile );
		String json = callServlet( query, input );
//		System.out.println( json );
		return json;
	}
	public String callServlet( String query, String input ) {
		String urlString = "http://127.0.0.1:8888/acode/shopServlet";
		StringBuffer sb = new StringBuffer();
		try {
			URL url = new URL( urlString );
			HttpURLConnection uc = (HttpURLConnection)url.openConnection();
			uc.setDoOutput( true );
			uc.setRequestMethod( "POST" );
			
			String parameterString = new String("query=" + query );
			if ( input != null ) {
				parameterString = parameterString + "&input=" + input;
			}
			PrintWriter printWriter = new PrintWriter( uc.getOutputStream() );
			printWriter.print( parameterString );
			printWriter.close();

			InputStream is = uc.getInputStream();
			BufferedReader reader = new BufferedReader( new InputStreamReader( is, "UTF-8" ) );
			String s;

			while ( ( s = reader.readLine() ) != null ) {
				sb.append( s );
			}
			reader.close();
			
			uc.disconnect();

		} catch (Exception e) {
			e.printStackTrace();
		}
		String json = sb.toString();
		return json;
	}
}
