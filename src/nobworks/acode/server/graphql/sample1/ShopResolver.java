package nobworks.acode.server.graphql.sample1;

import java.util.List;

import graphql.kickstart.tools.GraphQLMutationResolver;
import graphql.kickstart.tools.GraphQLQueryResolver;
import nobworks.acode.server.util.HeapUtil;

public class ShopResolver implements GraphQLQueryResolver, GraphQLMutationResolver {

	//--------------------------------------------
	// Mutation
	//--------------------------------------------
	public ShopMaster writeShop( String shopNo, String shopName, String managerName, String updateDate, double weight, Staff[] staffList ) {
	/*	System.out.println( "Enter writeShop()" );
		System.out.println( "shopNo=" + shopNo );
		System.out.println( "weight=" + weight );
		System.out.println( "staffList=" + staffList ); */
		
		ShopMaster shop = new ShopMaster( shopNo, shopName, managerName, updateDate );
		
		// writeShop.gql で staffList の配列を指定しない場合、staffList は null になる。
		shop.setStaffList( staffList );
		
		SampleStorage.setShop( shop );
		
		return shop;
	}
	public ShopMaster[] writeShops( ShopMasterInput[] para ) {
//		System.out.println( "Enter writeShops()" );
		if ( para == null || para.length == 0 ) {
			return null;
		}
		ShopMaster[] shopList = new ShopMaster[ para.length ];
		int shopCount = 0;
		for ( ShopMasterInput input : para ) {
//			System.out.println( "shopNo=" + input.getShopNo() );
			
			ShopMaster shop = new ShopMaster( input );
			shopList[ shopCount ] = shop;

			Staff[] staffInputList = input.getStaffList();
			if ( staffInputList != null ) {
				Staff[] staffList = new Staff[ staffInputList.length ];
				shop.setStaffList( staffList );
				int staffCount = 0;
				for ( Staff staffInput : staffInputList ) {
	//				System.out.println( "\tstaffNo=" + staffInput.getStaffNo() );
					Staff staff = new Staff( staffInput );
					staffList[ staffCount ] = staff;
					staffCount++;
				}
			}
			shopCount++;
		}
		for ( ShopMaster shop : shopList ) {
			SampleStorage.setShop( shop );
		}
		
		return shopList;
	}
	//--------------------------------------------
	// Query
	//--------------------------------------------
	public ShopMaster getShop( String shopNo ) {
//		System.out.println( "shopNo=" + shopNo );
		ShopMaster shop = SampleStorage.getShop( shopNo );
		return shop;
	}
	public ShopMaster[] getAllShops() {
//		System.out.println( "shopNo=" + shopNo );
		List<ShopMaster> shopList = SampleStorage.getShopList();
		ShopMaster[] shops = shopList.toArray( new ShopMaster[ shopList.size() ] );
		return shops;
	}
}
