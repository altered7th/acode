package nobworks.acode.server.graphql.sample1;

import nobworks.acode.server.graphql.ZGraphQLServlet;

public class ShopServlet extends ZGraphQLServlet {

	public ShopServlet() {
		super( "schema/shopmaster.graphqls", new ShopResolver() );
	}
}
