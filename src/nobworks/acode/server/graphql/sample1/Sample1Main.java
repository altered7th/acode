package nobworks.acode.server.graphql.sample1;

import java.util.List;

import graphql.kickstart.tools.GraphQLResolver;
import nobworks.acode.server.graphql.ZGraphQL;
import nobworks.acode.server.util.TestFileReader;

public class Sample1Main extends ZGraphQL {

	public static void main( String[] args ) {
		Sample1Main main = new Sample1Main();
		
//		main.runThread();
		main.runShop();
//		main.runCsv();
		
//		for ( int i=0;i<10;i++ ) {
//		main.runShop();
//		main.runCsv();
//		}
		
	}
	public void runShop() {
		ShopSample shopSample = new ShopSample();
		shopSample.run();
		System.out.println( "after shopSample.run()" );
		/*
		for ( int i=0;i<10;i++ ) {
			System.out.println( "------:" + i );
			shopSample.run();
		}
		*/
	}
	public void runCsv() {
		CsvSample csvSample = new CsvSample();
		csvSample.run();
	}
	public void runThread() {
		ShopThread t1 = new ShopThread( 0 );
		ShopThread t2 = new ShopThread( 1000 );
		ShopThread t3 = new ShopThread( 1000 );
		t1.start();
		t2.start();
		t3.start();
	}
	
	
	class ShopThread extends Thread {
		private long sleep;
		public ShopThread( long sleep ) {
			this.sleep = sleep;
		}
		public void run() {
			try {
				Thread.sleep( sleep );
				ShopSample shopSample = new ShopSample();
				shopSample.run();
			} catch ( Exception e ) {
				e.printStackTrace();
			}
		}
	}
}
