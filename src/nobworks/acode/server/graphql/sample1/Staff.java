package nobworks.acode.server.graphql.sample1;

public class Staff {

	private String staffNo;
	private String staffName;
	private String email;
	private String address;
	private String tel;
	private int age;
	private double height;
	private double weight;

	public Staff() {}
	public Staff( String staffNo, String staffName) {
		this.staffNo = staffNo;
		this.staffName = staffName;
	}
	public Staff( Staff input ) {
		this.staffNo = input.getStaffNo();
		this.staffName = input.getStaffName();
	}
	public String getStaffNo() {
		return staffNo;
	}
	public void setStaffNo( String staffNo ) {
		this.staffNo = staffNo;
	}
	public String getStaffName() {
		return staffName;
	}
	public void setStaffName( String staffName ) {
		this.staffName = staffName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail( String email ) {
		this.email = email;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress( String address ) {
		this.address = address;
	}
	public String getTel() {
		return tel;
	}
	public void setTel( String tel ) {
		this.tel = tel;
	}
	public int getAge() {
		return age;
	}
	public void setAge( int age ) {
		this.age = age;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight( double height ) {
		this.height = height;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight( double weight ) {
		this.weight = weight;
	}
}
