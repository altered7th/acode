package nobworks.acode.server.graphql.sample1;

/**
 * @see ShopMasterInput
 * ShopMaster と同様に StaffInput と Staff の構成は同じため、
 * ShopMaster 内の Staff をそのまま使用できる。
 */
public class StaffInput extends Staff {
}
