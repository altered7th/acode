package nobworks.acode.server.graphql.sample1;

public class ShopMaster {

	private String shopNo;
	private String shopName;
	private String managerName;
	private String updateDate;
	private Staff[] staffList;

	public ShopMaster() {
	}

	public ShopMaster( String shopNo, String shopName, String managerName, String updateDate ) {
		this.shopNo = shopNo;
		this.shopName = shopName;
		this.managerName = managerName;
		this.updateDate = updateDate;
	}
	public ShopMaster( ShopMasterInput input ) {
		this.shopNo = input.getShopNo();
		this.shopName = input.getShopName();
		this.managerName = input.getManagerName();
		this.updateDate = input.getUpdateDate();
	}

	public String getShopNo() {
		return shopNo;
	}

	public void setShopNo( String shopNo ) {
		this.shopNo = shopNo;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName( String shopName ) {
		this.shopName = shopName;
	}

	public String getManagerName() {
		return managerName;
	}

	public void setManagerName( String managerName ) {
		this.managerName = managerName;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate( String updateDate ) {
		this.updateDate = updateDate;
	}

	public Staff[] getStaffList() {
		return staffList;
	}

	public void setStaffList( Staff[] staffList ) {
		this.staffList = staffList;
	}
}
