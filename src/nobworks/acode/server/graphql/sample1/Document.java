package nobworks.acode.server.graphql.sample1;

public class Document {
	
	private String docNo;
	private String csv;

	public Document() {
		
	}
	public Document( String docNo, String csv ) {
		this.docNo = docNo;
		this.csv = csv;
	}
	public String getDocNo() {
		return docNo;
	}
	public void setDocNo( String docNo ) {
		this.docNo = docNo;
	}
	public String getCsv() {
		return csv;
	}
	public void setCsv( String csv ) {
		this.csv = csv;
	}
}
