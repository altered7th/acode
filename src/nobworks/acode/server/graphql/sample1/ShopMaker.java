package nobworks.acode.server.graphql.sample1;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import nobworks.zcore.zclass.ZUtil;

// writShopsVariables.gql 用に大量データを作成する
public class ShopMaker {

	private final String tab1 = "\t";
	private final String tab2 = tab1 + "\t";
	private final String tab3 = tab2 + "\t";
	private final String tab4 = tab3 + "\t";
	private final String tab5 = tab4 + "\t";
	
	private final static int SHOP_MAX = 10000;
	private final static int STAFF_MAX = 50;
	
	private FileWriter fileWriter;
	
	public static void main( String[] args ) {
		ShopMaker maker = new ShopMaker();
		maker.start();
	}
	public void start() {
		try {
			File file = new File( "src/nobworks/acode/server/graphql/sample1/bigData.gql" );
			fileWriter = new FileWriter( file );
		} catch ( IOException e ) {
			e.printStackTrace();
			System.exit( -999 );
		}
		print( "{" );
		print( tab1 + q2( "input" ) + ": [" );
		
		for ( int i=1;i<=SHOP_MAX;i++ ) {
			createShop( i );
		}
		
		print( tab1 + "]" );
		print( "}" );
		try {
			fileWriter.close();
		} catch ( IOException e ) {
			e.printStackTrace();
			System.exit( -998 );
		}
	}
	private void createShop( int shopCount ) {
		String comma = ( shopCount < SHOP_MAX ) ? "," : "";
		print( tab2 + "{" );

		print( tab3 + q2( "shopNo" ) + " : " + q2( "shop-" + zero( shopCount ) ) + "," );
		print( tab3 + q2( "shopName" ) + " : " + q2( "shop名-" + zero( shopCount ) ) + "," );
		print( tab3 + q2( "managerName" ) + " : " + q2( "manager名-" + zero( shopCount ) ) + "," );
		print( tab3 + q2( "updateDate" ) + " : " + q2( "20220204" ) + "," );
		
		createStaff( shopCount );

		print( tab2 + "}" + comma );
	}
	private void createStaff( int shopCount ) {
		print( tab3 + q2( "staffList" ) + ": [" );

		for ( int i=1;i<=STAFF_MAX;i++ ) {
			createStaffBody( shopCount, i );
		}
		print( tab3 + "]" );
	}
	private void createStaffBody( int shopCount, int staffCount ) {
		String comma = ( staffCount < STAFF_MAX ) ? "," : "";
		print( tab4 + "{" );
		String value = zero( shopCount ) + "-" + zero( staffCount );
		print( tab5 + q2( "staffNo" ) + " : " + q2( "staffNo-" + value ) + "," );
		print( tab5 + q2( "staffName" ) + " : " + q2( "staffName-" + value ) );
		print( tab4 + "}" + comma );	
	}
	private void print( String s ) {
//		System.out.println( s );
		try {
			fileWriter.write( s + "\n" );
		} catch ( IOException e ) {
			e.printStackTrace();
		}
	}
	private static String zero( int value ) {
		return ZUtil.right( "000" + value, 3 );
	}
	private static String q2( String value ) {
		return ZUtil.quote2( value );
	}
}
