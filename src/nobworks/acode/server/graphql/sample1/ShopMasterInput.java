package nobworks.acode.server.graphql.sample1;

/**
 * ShopMaster と ShopMasterInput は GraphQL では異なるオブジェクトだが
 * Java の Resolver 内では継承しても問題なく扱うことができている。
 */
public class ShopMasterInput extends ShopMaster {
}
