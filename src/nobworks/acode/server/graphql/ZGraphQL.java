package nobworks.acode.server.graphql;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.kickstart.tools.GraphQLResolver;
import graphql.kickstart.tools.SchemaParser;
import graphql.schema.GraphQLSchema;
import nobworks.acode.server.util.HeapUtil;

public class ZGraphQL {
	
	private static Map<String, GraphQL> cacheMap = new HashMap<String, GraphQL>();
	private String schema;
	private GraphQLResolver<?> resolver;
	
	public ZGraphQL() {}
	
	public ZGraphQL( String schema, GraphQLResolver<?> resolver ) {
		this.schema = schema;
		this.resolver = resolver;
		createGraphQL( schema, resolver );
	}
	private GraphQL createGraphQL( String schema, GraphQLResolver<?> resolver ) {
		GraphQL graphQL = cacheMap.get( schema );
		if ( graphQL == null ) {
			GraphQLSchema graphQLSchema = SchemaParser.newParser()
					.file( schema )
					.resolvers( resolver )
					.build()
					.makeExecutableSchema();
					;
			graphQL = GraphQL.newGraphQL( graphQLSchema ).build();
	
			cacheMap.put( schema, graphQL );
//			System.out.println( "========> cache set:" + schema );
		}
		return graphQL;
	}

	public String runGql( String gql ) {
		return runGql( schema, resolver, gql, null );
	}
	public String runGql( String gql, String variablesScript ) {
		return runGql( schema, resolver, gql, variablesScript );
	}

	public String runGql( String schemaFile, GraphQLResolver<?> resolver, String gql ) {
		return runGql( schemaFile, resolver, gql, null );
	}
	public String runGql( String schemaFile, GraphQLResolver<?> resolver, String gql, String variablesScript ) {
//		System.out.println( "Enter runGql" );
		long start = System.currentTimeMillis();

		ExecutionResult executionResult;
		GraphQL graphQL = createGraphQL( schemaFile, resolver );
		
		if ( variablesScript != null ) {
//			System.out.println( "variables=" + variablesScript );
			Map<String, Object> variablesMap = getVariablesMap( variablesScript );
			ExecutionInput input = ExecutionInput.newExecutionInput()
				.query( gql )
				.variables( variablesMap )
				.build();
			executionResult = graphQL.execute( input );
		} else {
			executionResult = graphQL.execute( gql );
		}
		String json = getJson( executionResult.toSpecification() );

		long end = System.currentTimeMillis();
		long ms = end - start;
		if ( ms > 0 ) {
			String heap = ms + "ms " + HeapUtil.getHeap();
//			System.out.println( heap + " json=" + json.length() );
		}

		return json;
	}
	private String getJson( Map<String, Object> map ) {
		ObjectMapper om = new ObjectMapper();
		String json = null;
		try {
			json = om.writeValueAsString( map.get( "data" ) );
		} catch (Exception e) {
			e.printStackTrace();
		}	
		return json;
	}
	// variables GQL script を map に変換する
	private Map<String, Object> getVariablesMap( String variablesScript ) {
		Map<String, Object> variablesMap = new HashMap<String, Object>();
		ObjectMapper mapper = new ObjectMapper();
		try {
			variablesMap = mapper.readValue( variablesScript, new TypeReference<Map<String, Object>>() {
			} );
			//		System.out.println( "variablesMap=" + variablesMap );
		} catch ( Exception e ) {
			e.printStackTrace();
		}
		return variablesMap;
	}
}
