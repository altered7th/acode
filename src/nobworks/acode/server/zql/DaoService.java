package nobworks.acode.server.zql;

import java.util.List;

import nobworks.zcore.zclass.ZObject;
import nobworks.zql.client.ZqlDto;
import nobworks.zql.server.ZqlService;

public class DaoService extends ZqlService {

	public static void main( String[] args ) {
		DaoService dao = new DaoService();
		dao.testWrite( null );
//		dao.testGetRow( null );
//		dao.testGetRows( null );
//		dao.testFileSql( null );
	}
	
	public ZObject testWrite( ZObject para ) {
		String sql = "delete from TEST1";
		executeSql( sql );
		sql = getSql( "test0.sql", getClass() )
			.replaceAll( "<id>", "001" )
			.replaceAll( "<name>", "ああああ" )
			.replaceAll( "<age>", "100" )
			;
		executeSql( sql );
		
		sql = getSql( "test0.sql", getClass() )
				.replaceAll( "<id>", "002" )
				.replaceAll( "<name>", "いいいい" )
				.replaceAll( "<age>", "50" )
				;
		executeSql( sql );
	
		sql = getSql( "test0.sql", getClass() )
				.replaceAll( "<id>", "003" )
				.replaceAll( "<name>", "うううう" )
				.replaceAll( "<age>", "30" )
				;
		executeSql( sql );
		return para;
	}
	public ZObject testGetRows( ZObject para ) {
		String sql = "select * from TEST1";
		System.out.println( sql );
		List<ZqlDto> rows = getRows( sql );
		for ( ZqlDto dto : rows ) {
			System.out.println( "ID=" + dto.get( "ID" ) );
			System.out.println( "NAME=" + dto.get( "NAME" ) );
			System.out.println( "AGE=" + dto.getInteger( "AGE" ) );
		}
		para.set( "dtoList", rows );
		return para;
	}
	public ZObject testGetRow( ZObject para ) {
		String sql = "select * from TEST1";
		System.out.println( sql );
		ZqlDto dto = getRow( sql );
		if ( dto != null ) {
			System.out.println( "ID=" + dto.get( "ID" ) );
			System.out.println( "NAME=" + dto.get( "NAME" ) );
			System.out.println( "AGE=" + dto.getInteger( "AGE" ) );
		}
		para.set( "dto", dto );
		return para;
	}
	public ZObject testFileSql( ZObject para ) {
		String where = "where ID='002'";
		String sql = getSql( "test3.sql", getClass() )
			.replaceAll( "<where>", where )
			;
		List<ZqlDto> rows = getRows( sql );
		for ( ZqlDto dto : rows ) {
			System.out.println( "ID=" + dto.get( "ID" ) );
			System.out.println( "NAME=" + dto.get( "NAME" ) );
			System.out.println( "AGE=" + dto.getInteger( "AGE" ) );
		}
		para.set( "dtoList", rows );
		return para;
	}
}
