package nobworks.acode.server;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class ContentsFactory {
	
	private static Map<String,String> pageMap = new HashMap<String,String>();

	public static String getContents( String page ) {
		
		String contents = pageMap.get( page );
		
		if ( contents == null ) {
			Scanner scanner = new Scanner( ContentsFactory.class.getResourceAsStream( page ), "UTF-8" );
			StringBuilder result = new StringBuilder( "" );
			while( scanner.hasNextLine() ) {
				String line = scanner.nextLine();
				result.append( line ).append( "\n" );
			}
			contents = result.toString();
			scanner.close();
			
			pageMap.put( page, contents );
		}
		
		return new String( contents );
	}
	public static String getContents() {
		return getContents( "base.html" );
	}
	
	public static void main( String[] args ) {
		String text = ContentsFactory.getContents();
		System.out.println( text );
	}
}
