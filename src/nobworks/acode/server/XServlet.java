package nobworks.acode.server;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class XServlet extends HttpServlet {

	public void doGet( HttpServletRequest req , HttpServletResponse res ) throws IOException,ServletException {
		String contents = ContentsFactory.getContents();
		contents = contents.replace( "%oldName%", "" );
		out( res, contents );
		
	}
	public void doPost( HttpServletRequest req , HttpServletResponse res ) throws IOException,ServletException {
		String page = req.getParameter( "nextPage" );
		String contents = ContentsFactory.getContents( page );
		String name = req.getParameter( "name" );
		contents = contents.replace( "%oldName%", name );
		out( res, contents );
	}
	
	private void out( HttpServletResponse res, String contents ) throws IOException,ServletException {
		res.setContentType( "text/html; charset=UTF-8" );
		PrintWriter out = res.getWriter();
		out.println( contents );
	}
}
