練習用のダミープロジェクトなので注意して下さい。詳細は下記 URL を参照。

<https://drive.google.com/drive/u/0/folders/1UGS8fbgS2wz8VWsZ7MRuOZs25hI915wV/>

graphql-java 関連ライブラリ(Qiita)

<https://qiita.com/ma2ge/items/af0f60a3fd30fdb53cb3/>

Web アプリケーション起動時にデータベースを起動する方法

<https://fumidzuki.com/knowledge/1694/>


H2 Database Engine (H2DB) の環境構築

<https://qiita.com/syany/items/fefb5d886f3a723036e1>

H2 コンソールは h2.sh 実行後にこのサーバーの JDBC URL を指定して参照する。