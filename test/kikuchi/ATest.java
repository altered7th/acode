package kikuchi;

public class ATest {

	public static void main( String[] args ) {
		ATest aTest = new ATest();
		int count = 1000;
		aTest.test1( count );
		aTest.test2( count );
	}
	
	public void test1( int count ) {
		long startTime = System.currentTimeMillis();
		
		StringBuffer sb = new StringBuffer();
		String s = "xxxxxxxxxxxxx";

//		int result = 0;
        for (int i = 0; i < count; i++) {
//            result += 1;
        	sb.append( s );
        }

        long endTime = System.currentTimeMillis();
        
        System.out.println("----StringBuffer" );
        System.out.println("開始時刻：" + startTime + " ms");
        System.out.println("終了時刻：" + endTime + " ms");
        System.out.println("処理時間：" + (endTime - startTime) + " ms");
	}
	public void test2( int count ) {
		long startTime = System.currentTimeMillis();
		
		StringBuilder sb = new StringBuilder();
		String s = "xxxxxxxxxxxxx";

//		int result = 0;
        for (int i = 0; i < count; i++) {
//            result += 1;
        	sb.append( s );
        }

        long endTime = System.currentTimeMillis();
        
        System.out.println("----StringBuilder" );
        System.out.println("開始時刻：" + startTime + " ms");
        System.out.println("終了時刻：" + endTime + " ms");
        System.out.println("処理時間：" + (endTime - startTime) + " ms");
	}
}
