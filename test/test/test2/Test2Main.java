package test.test2;

import java.util.Collections;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.kickstart.tools.SchemaParser;
import graphql.schema.GraphQLSchema;
import nobworks.acode.server.util.TestFileReader;
import test.test1.Test1Resolver;

// input の簡易テスト
public class Test2Main {
	public static void main( String[] args ) {
		Test2Main x = new Test2Main();
		x.test1();
//		x.test2();
	}
	public void test2() {
		TestFileReader reader = new TestFileReader( "/test/test2/" );

		ExecutionResult executionResult;
		GraphQLSchema graphQLSchema = SchemaParser.newParser()
				.file( "schema/test2.graphqls" )
				.resolvers( new Test2Resolver() )
				.build()
				.makeExecutableSchema();
				;
//		if ( true ) { System.exit(0); }
		GraphQL graphQL = GraphQL.newGraphQL( graphQLSchema ).build();
		String gql = reader.getContents( "query.gql" );
		System.out.println( "gql=" + gql );
		
		/*
		String varScript = FileReader.getContents( "test2/variables.gql" );
		Map<String,Object> variables = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			variables = mapper.readValue( varScript, new TypeReference<Map<String, Object>>(){});
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		ExecutionInput input = ExecutionInput.newExecutionInput().
			    query( gql ).
			    variables( variables ).
//			    root( context ).
//			    context( context ).
			    build();
		*/
		
		executionResult = graphQL.execute( gql );
		System.out.println( executionResult.toSpecification() );
	}
	public void test1() {
		TestFileReader reader = new TestFileReader( "/test/test2/" );

		ExecutionResult executionResult;
		GraphQLSchema graphQLSchema = SchemaParser.newParser()
				.file( "schema/test2.graphqls" )
				.resolvers( new Test2Resolver() )
				.build()
				.makeExecutableSchema();
				;
//		if ( true ) { System.exit(0); }
		GraphQL graphQL = GraphQL.newGraphQL( graphQLSchema ).build();
		String gql = reader.getContents( "queryInput.gql" );
		System.out.println( "[gql]=" + gql );
		
		String varScript = reader.getContents( "variables.gql" );
		System.out.println( "[variables]=" + varScript );
		Map<String,Object> variables = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			variables = mapper.readValue( varScript, new TypeReference<Map<String, Object>>(){});
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		ExecutionInput input = ExecutionInput.newExecutionInput().
			    query( gql ).
//			    root( context ).
//			    context( new Object() ).	//Basically any class you want
			    variables( variables ).
			    build();
		
		executionResult = graphQL.execute( input );
//		System.out.println( executionResult.toSpecification() );
//		System.out.println( executionResult.toSpecification().get( "data" ) );
		
		ObjectMapper om = new ObjectMapper();
		String json = null;
		try {
			json = om.writeValueAsString( executionResult.toSpecification().get( "data" ) );
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println( "json=>" + json );
	}
}
