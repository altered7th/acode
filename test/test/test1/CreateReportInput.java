package test.test1;

public class CreateReportInput {

	private String reportNo;
	private String memberName;
	private String createDate;
	private int sum;
	
	public CreateReportInput() {}
	public CreateReportInput( String reportNo, String memberName, String createDate, int sum ) {
		this.reportNo = reportNo;
		this.memberName = memberName;
		this.createDate = createDate;
		this.sum = sum;
	}

	public String getReportNo() {
		return reportNo;
	}

	public void setHeaderNo( String reportNo ) {
		this.reportNo = reportNo;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName( String memberName ) {
		this.memberName = memberName;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate( String createDate ) {
		this.createDate = createDate;
	}

	public int getSum() {
		return sum;
	}

	public void setSum( int sum ) {
		this.sum = sum;
	}
}
