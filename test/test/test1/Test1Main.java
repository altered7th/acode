package test.test1;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.kickstart.tools.GraphQLResolver;
import graphql.kickstart.tools.SchemaParser;
import graphql.schema.GraphQLSchema;
import nobworks.acode.server.util.TestFileReader;

public class Test1Main {

	public static void main( String[] args ) {
		Test1Main x = new Test1Main();
		String json;
		TestFileReader reader = new TestFileReader( "/test/test1/" );
		
		String schemaFile = "schema/test1.graphqls";
		GraphQLResolver<?> resolver = new Test1Resolver();

		// 配列更新テスト
		String query = reader.getContents( "writeReports.gql" );
		String variables = reader.getContents( "writeReportsVariables.gql" );
		json = x.runGql( schemaFile, resolver, query, variables );

		query = reader.getContents( "write.gql" );
//		json = x.runGql( schemaFile, resolver, query );

		// getReport(), getReport2() の切り替わり確認
		query = reader.getContents( "getReport.gql" );
//		json = x.runGql( schemaFile, resolver, query );

		// 全データを配列で返す
		query = reader.getContents( "getReports.gql" );
//		json = x.runGql( schemaFile, resolver, query );
		
		// variables 引数の Query
		query = reader.getContents( "getReport3.gql" );
		variables = reader.getContents( "getReport3Variables.gql" );
//		json = x.runGql( schemaFile, resolver, query, variables );

		System.out.println( "json=>" + json );
	}
	
	public String runGql( String schemaFile, GraphQLResolver<?> resolver, String gql ) {
		return runGql( schemaFile, resolver, gql, null );
	}
	public String runGql( String schemaFile, GraphQLResolver<?> resolver, String gql, String variablesScript ) {
		ExecutionResult executionResult;
		GraphQLSchema graphQLSchema = SchemaParser.newParser()
				.file( schemaFile )
				.resolvers( resolver )
				.build()
				.makeExecutableSchema();
				;
		GraphQL graphQL = GraphQL.newGraphQL( graphQLSchema ).build();
		
		if ( variablesScript != null ) {
			System.out.println( "variables=" + variablesScript );
			Map<String, Object> variablesMap = getVariablesMap( variablesScript );
			ExecutionInput input = ExecutionInput.newExecutionInput()
				.query( gql )
				.variables( variablesMap )
				.build();
			executionResult = graphQL.execute( input );
		} else {
			executionResult = graphQL.execute( gql );
		}
		String json = getJson( executionResult.toSpecification() );
		return json;
	}
	private String getJson( Map<String, Object> map ) {
		ObjectMapper om = new ObjectMapper();
		String json = null;
		try {
			json = om.writeValueAsString( map.get( "data" ) );
		} catch (Exception e) {
			e.printStackTrace();
		}	
		return json;
	}
	// variables GQL script を map に変換する
	private Map<String, Object> getVariablesMap( String variablesScript ) {
		Map<String, Object> variablesMap = new HashMap<String, Object>();
		ObjectMapper mapper = new ObjectMapper();
		try {
			variablesMap = mapper.readValue( variablesScript, new TypeReference<Map<String, Object>>() {
			} );
			//		System.out.println( "variablesMap=" + variablesMap );
		} catch ( Exception e ) {
			e.printStackTrace();
		}
		return variablesMap;
	}
}
