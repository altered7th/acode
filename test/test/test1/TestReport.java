package test.test1;

public class TestReport {

	private String reportNo;
	private String memberName;
	private String createDate;
	private int sum;
	
	public TestReport() {}
	public TestReport( String reportNo, String memberName, String createDate, int sum ) {
		this.reportNo = reportNo;
		this.memberName = memberName;
		this.createDate = createDate;
		this.sum = sum;
	}
	public TestReport( CreateReportInput in ) {
		this.reportNo = in.getReportNo();
		this.memberName = in.getMemberName();
		this.createDate = in.getCreateDate();
		this.sum = in.getSum();
	}

	public String getReportNo() {
		return reportNo;
	}

	public void setHeaderNo( String reportNo ) {
		this.reportNo = reportNo;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName( String memberName ) {
		this.memberName = memberName;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate( String createDate ) {
		this.createDate = createDate;
	}

	public int getSum() {
		return sum;
	}

	public void setSum( int sum ) {
		this.sum = sum;
	}
}
