package test.test1;

import graphql.kickstart.tools.GraphQLMutationResolver;
import graphql.kickstart.tools.GraphQLQueryResolver;

public class Test1Resolver implements GraphQLQueryResolver, GraphQLMutationResolver {

	//--------------------------------------------
	// Query
	//--------------------------------------------
	public TestReport getReport( String reportNo ) {
		System.out.println( "Enter Test1Resolver.getReport()" );
		
		TestReport testReport = new TestReport( reportNo, "XXX", "99999999", 999 );
		return testReport;
	}
	public TestReport getReport2( String reportNo ) {
		System.out.println( "Enter Test1Resolver.getReport2()" );
		
		TestReport testReport = new TestReport( reportNo, "YYYY", "99999999", 999 );
		return testReport;
	}
	
	// 引数無しで全データを配列で返す
	public TestReport[] getAllReports() {
		TestReport[] reports = new TestReport[2];
		reports[0] = new TestReport( "rep-1", "YYYY", "99999999", 999 );
		reports[1] = new TestReport( "rep-2", "ZZZZZ", "222222", 222 );
		return reports;
	}
	
	// variables 引数の場合は Mutation 側に定義する
	public TestReport getReport3( CreateReportInput para ) {
		System.out.println( "Enter Test1Resolver.getReport3()" );
		
		TestReport testReport = new TestReport( "rep-XXX", "XXX-XXX-XXXX", "99999999", 999 );
		return testReport;
	}
	//--------------------------------------------
	// Mutation
	//--------------------------------------------
	public TestReport write( String reportNo, String memberName, String createDate, int sum ) {
		
		System.out.println( "Enter Test1Resolver.write()" );
		
		TestReport testReport = new TestReport( reportNo, memberName, createDate, sum );
		return testReport;
	}

	
	public TestReport[] createReports( CreateReportInput[] reports ) {
		System.out.println( "**** Enter createReports:" + reports.length );
		for ( CreateReportInput input : reports ) {
			System.out.println( "reportNo=" + input.getReportNo() );
		}
		TestReport[] out = new TestReport[ reports.length ];
		int i= 0;
		for ( CreateReportInput input : reports ) {
			out[i++] = new TestReport( input );
		}
		return out;
	}
	
}
