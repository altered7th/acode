package test.hello.types;

public enum Episode {
  NEWHOPE,
  EMPIRE,
  JEDI
}
