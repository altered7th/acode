package test.hello;

import java.util.Map;

import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.kickstart.tools.SchemaParser;
import graphql.schema.GraphQLSchema;
import nobworks.acode.server.util.TestFileReader;
import test.hello.book.BookResolver;
import test.hello.resolvers.Mutation;
import test.hello.resolvers.Query;
import test.tenpo.ShopOpenResolver;

public class HelloTools {

	public static void main( String[] args ) {

		System.out.println( "Enter HelloTools" );
		HelloTools x = new HelloTools();
		
		// Star Wars : swapi.graphqls
//		x.testDefault();

		// Books : book.graphqls
//		x.test1();
		
		// 店舗 : shopopen.graphqls
		x.testTenpo();
		
		System.out.println( "Exit HelloTools" );
	}
	//-------------------------------------------------------------------------------------------------
	public void testTenpo() {
		ExecutionResult executionResult;
		GraphQLSchema graphQLSchema = SchemaParser.newParser()
				.file( "schema/shopopen.graphqls" )
				.resolvers( new ShopOpenResolver() )
				.build()
				.makeExecutableSchema();
				;
		GraphQL graphQL = GraphQL.newGraphQL( graphQLSchema ).build();

		TestFileReader reader = new TestFileReader( "/test/hello/" );
		String gql = reader.getContents( "openinfo1.gql" );
		System.out.println( gql );
		executionResult = graphQL.execute( gql );
		System.out.println( executionResult.toSpecification() );
	}
	
	//-------------------------------------------------------------------------------------------------
	public void test1() {
		ExecutionResult executionResult;
		GraphQLSchema graphQLSchema = SchemaParser.newParser()
				.file( "schema/book.graphqls" )
				.resolvers( new BookResolver() )
				.build()
				.makeExecutableSchema();
				;
		GraphQL graphQL = GraphQL.newGraphQL( graphQLSchema ).build();

		executionResult = graphQL.execute(  "mutation {registerBook( id:\"5\", name:\"xxx\", pageCount:999 ) {id name pageCount } }" );
		System.out.println( "登録：" + executionResult.toSpecification() );

		executionResult = graphQL.execute( "{ books{ id name } } " );
		System.out.println( "取得：" + executionResult.toSpecification() );

		executionResult = graphQL.execute( "{ bookById(id:\"2\") { id name } } " );
		System.out.println( "検索：" + executionResult.toSpecification() );
//		Map<String, Object> toSpecificationResult = executionResult.toSpecification();
//		Map<String, Object> dataMap = (Map<String, Object>)toSpecificationResult.get( "data" );
		

	}
	//-------------------------------------------------------------------------------------------------
	public void testDefault() {
		GraphQLSchema graphQLSchema = SchemaParser.newParser()
				.file( "schema/swapi.graphqls" )
				.resolvers( new Query(), new Mutation() )
				.build()
				.makeExecutableSchema();

		GraphQL graphQL = GraphQL.newGraphQL( graphQLSchema ).build();

		ExecutionResult executionResult = graphQL.execute( "{ droid(id: \"2001\") { name } } " );

		System.out.println( "Query result: " );
		System.out.println( executionResult.toSpecification() );
	}
}
