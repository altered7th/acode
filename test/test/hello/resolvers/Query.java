package test.hello.resolvers;

import java.util.List;
import java.util.Map;

import graphql.kickstart.tools.GraphQLQueryResolver;
import test.hello.CharacterRepository;
import test.hello.types.Character;
import test.hello.types.Droid;
import test.hello.types.Episode;
import test.hello.types.Human;

public class Query implements GraphQLQueryResolver {

  private CharacterRepository characterRepository = new CharacterRepository();

  public Character hero(Episode episode) {
    return episode != null
        ? characterRepository.getHeroes().get(episode)
        : characterRepository.getCharacters().get("1000");
  }

  public Human human(String id) {
    return (Human)
        characterRepository.getCharacters().values().stream()
            .filter(character -> character instanceof Human && character.getId().equals(id))
            .findFirst()
            .orElseThrow(IllegalStateException::new);
  }

  public Droid droid(String id) {
    return (Droid)
        characterRepository.getCharacters().values().stream()
            .filter(character -> character instanceof Droid && character.getId().equals(id))
            .findFirst()
            .orElseThrow(IllegalStateException::new);
  }

  public Character character(String id) {
    return characterRepository.getCharacters().get(id);
  }
  
  /*
  public Map<String, Character> allCharacter() {
	  return characterRepository.getCharacters();
  }
  */
}
