package test.hello.book;

import java.util.List;

public interface DataProvider {
	List<Book> books();

	Book bookById( String bookId );
}
