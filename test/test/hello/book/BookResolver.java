package test.hello.book;

import java.util.List;

import org.reactivestreams.Publisher;

import graphql.GraphQLError;
import graphql.kickstart.tools.GraphQLMutationResolver;
import graphql.kickstart.tools.GraphQLQueryResolver;
import graphql.kickstart.tools.GraphQLSubscriptionResolver;

public class BookResolver implements GraphQLQueryResolver, GraphQLMutationResolver, GraphQLSubscriptionResolver {

	private final static DataProvider dataProvider = new ConstantDataProvider();
    private final static IBookProcessor bookProcessor = new BookInMemoryProcessor();
//    private IBookProcessor bookProcessor;

    /**
     * Query: Get all books.
     */
    public List<Book> books() {
    	System.out.println( "books() called" );
        return dataProvider.books();
    }

    /**
     * Query: Retrieve a book by id.
     */
    public Book bookById(String bookId) {
    	System.out.println( "bookById() called : bookId=" + bookId );
        return dataProvider.bookById(bookId);
    }

    /**
     * Mutation: Register a book.
     */
	public Book registerBook( String id, String name, int pageCount ) {
    	System.out.println( "registerBook() called : id=" + id );
        final Book book = new Book(id, name, pageCount);
        dataProvider.books().add(book);

        // Emit an event for subscription.
//        bookProcessor.emit(book);
        return book;
    }

    /**
     * Subscription: Publish an event that a book is registered.
     * Need to return Publisher on reactive-streams.
     */
    public Publisher<Book> subscribeBooks() {
        return bookProcessor.publish();
    }

    /**
     * Error handler. can handle an throwable that occurs in resolver execution.
     */
    GraphQLError handle(Throwable e) {
        System.err.println( "Failed to execute resolver." + e );
        throw new RuntimeException( "Failed to execute resolver." + e );
    }
}
