package test.hello.book;

public class Book {

	private String id;
	private String name;
	private int pageCount;
	
	public Book( String id, String name, int pageCount ) {
		this.id = id;
		this.name = name;
		this.pageCount = pageCount;
	}

	public String getId() {
		return id;
	}

	public void setId( String id ) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName( String name ) {
		this.name = name;
	}

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount( int pageCount ) {
		this.pageCount = pageCount;
	}
}
