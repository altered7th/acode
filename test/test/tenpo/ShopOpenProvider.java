package test.tenpo;

import java.util.List;

public interface ShopOpenProvider {

	List<OpenInfo> shops();
}
