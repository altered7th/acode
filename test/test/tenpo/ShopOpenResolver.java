package test.tenpo;

import java.util.List;

import graphql.kickstart.tools.GraphQLMutationResolver;
import graphql.kickstart.tools.GraphQLQueryResolver;

public class ShopOpenResolver implements GraphQLQueryResolver, GraphQLMutationResolver {

	private final static ShopOpenProvider shopOpenProvider = new MockShopOpenProvider();

	public List<OpenInfo> shops() {
		return shopOpenProvider.shops();
	}
	
	public OpenInfo getOpenInfo( String tenpo, String yyyymmdd ) {
		OpenInfo openInfo = new OpenInfo( "00001", "99991122", "0.1", "1234", 9999, "11111122222233333", "xxx" );
		return openInfo;
	}
	public OpenInfo getOpenInfo2( String tenpo, String yyyymmdd ) {
		OpenInfo openInfo = new OpenInfo( "00001", "99991122", "0.1", "1234", 9999, "11111122222233333", "xxx" );
		return openInfo;
	}
	
	public OpenInfo registerOpenInfo( String tenpo, String yyyymmdd, String version, String user, int cashierPrice, String operationTime, String csv) {
		
		System.out.println( "----> Enter registerOpenInfo" );
		OpenInfo openInfo = new OpenInfo( tenpo, yyyymmdd, version, user, cashierPrice, operationTime, csv );
		System.out.println( "-----" );
		System.out.println( csv );
		System.out.println( "-----" );
		return openInfo;
	}
	
	//---------
	// このリゾルバに使用するクラスを参照させなければならない。
	// .graphqls で定義されているクラスのメンバ変数は、ここで参照している XBook と一致する必要がある。
	public XBook getBookById( String id ) {
		return null;
	}
	
	// リゾルバと Bean のコードは最低限必要かも　-->> ローコード化
	// データ管理(H2) はノーコード化

}
