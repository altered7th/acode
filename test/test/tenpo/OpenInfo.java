package test.tenpo;

/**
 * 店舗開店データ
 */
public class OpenInfo {

	private String tenpo;
	private String yyyymmdd;
	private String version;
	private String user;
	private int cashierPrice;
	private String operationTime;
	private String csv;

	public OpenInfo( String tenpo, String yyyymmdd, String version, String user, int cashierPrice, String operationTime, String csv ) {
		this.tenpo = tenpo;
		this.yyyymmdd = yyyymmdd;
		this.version = version;
		this.user = user;
		this.cashierPrice = cashierPrice;
		this.operationTime = operationTime;
		this.csv = csv;
	}

	public String getTenpo() {
		return tenpo;
	}

	public void setTenpo( String tenpo ) {
		this.tenpo = tenpo;
	}

	public String getYyyymmdd() {
		return yyyymmdd;
	}

	public void setYyyymmdd( String yyyymmdd ) {
		this.yyyymmdd = yyyymmdd;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion( String version ) {
		this.version = version;
	}

	public String getUser() {
		return user;
	}

	public void setUser( String user ) {
		this.user = user;
	}

	public int getCashierPrice() {
		return cashierPrice;
	}

	public void setCashierPrice( int cashierPrice ) {
		this.cashierPrice = cashierPrice;
	}

	public String getOperationTime() {
		return operationTime;
	}

	public void setOperationTime( String operationTime ) {
		this.operationTime = operationTime;
	}
	
	public String getCsv() {
		return csv;
	}
	
	public void setCsv( String csv ) {
		this.csv = csv;
	}
}
